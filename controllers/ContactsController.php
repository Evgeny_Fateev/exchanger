<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 27.07.2017
 * Time: 18:07
 */

namespace app\controllers;


use app\models\Contacts;
use app\models\Feedback;
use Yii;
use yii\web\Controller;

class ContactsController extends Controller
{
    public function actionForm(){
        $form_fb = new Feedback();
        $contacts = Contacts::find()->one();
        if($form_fb->load(Yii::$app->request->post())){
            Yii::$app->mailer->compose()
                ->setFrom('zhenja94@bk.ru')
                ->setTo($form_fb->email)
                ->setSubject( $form_fb->name)
                ->setTextBody($form_fb->text)
                ->send();
            $form_fb->save();
            return $this->refresh();
        }
        return $this->render('form', compact('form_fb', 'contacts'));
    }

}