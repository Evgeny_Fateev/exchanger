<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 21.07.2017
 * Time: 14:35
 */

namespace app\controllers;

use app\models\Feedback;
use app\models\Orders;
use app\models\RedactionText;
use app\models\Sliders;
use app\models\SubscriptionNews;
use app\models\Transactions;
use app\models\User;
use Codeception\Lib\Interfaces\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\data\ActiveDataProvider;
use app\models\Posts;
use app\models\Reviews;
use Yii;
use yii\helpers\StringHelper;
use yii\web\UploadedFile;
use yii\data\Pagination;
class HomeController extends BehaviorsController
{

    public function actionNews(){

        $query = Posts::find();
        $pages = new Pagination(['totalCount'=> $query->count(), 'pageSize'=>5,
          'forcePageParam' => false, 'pageSizeParam' => false]);
        $news  = $query->offset($pages->offset)->limit($pages->limit)->all();
        $model = Posts::find()->orderBy(['date' =>SORT_DESC])->all();
        return $this->render('news', compact('news','pages','model'));
    }

    public function actionRedactionText(){

        $redaction = RedactionText::find()->where(['id' => 1])->one();
        if ($redaction->load(Yii::$app->request->post())) {
            $redaction->text;
            $redaction->save();
            $this->refresh();
        }else {
            false;
        }
        return $this->render('redaction-text',compact('redaction'));
    }
    public function actionSortNews($id){
        $this->layout=false;

        $model = Posts::findOne($id);

        return $this->render('sort-news', compact('model'));
    }

    public function actionPersonalArea(){
        $this->view->title = Yii::t('common','Личный кабинет');
        $user = User::find()->select('email')->all();
        $sub_news = SubscriptionNews::find()->where(['email' => $user])->one();
        if ($sub_news == null)
        {
            $is_subNews = 0;
        }
        else{
            $is_subNews = 1;
        }
        $sub_news_save = new SubscriptionNews();
        if ($sub_news_save->load(Yii::$app->request->post())){
            if ($sub_news_save->save()){
                $sub_news_save->email = \Yii::$app->mailer->compose()
                    ->setTo('zhenja1509@gmail.com')
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Подписка на новости')
                    ->setTextBody("На ваши новости подписался " .$sub_news->email)
                    ->send();
            }
            $this->refresh();
        }
        $order = new Orders();
        if($order->load(Yii::$app->request->post())) {
            $order->is_user = Yii::$app->user->identity->id;
            if(!isset($_POST['is_confirm'])){
                Yii::$app->session->setFlash('error', 'Вы не согласились на обработку данных.');
                return $this->refresh();
            }
            if($order->save()){
                Yii::$app->session->setFlash('success', 'Спасибо, ваша заявка принята.');
                $order->email = \Yii::$app->mailer->compose()
                    ->setTo('zhenja1509@gmail.com')
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Заявка на '. $order->type)
                    ->setTextBody("Пользователь $order->name,  email: $order->email, телефон: $order->phone оставил заявку на: 
                        $order->type,  платежной системой $order->payment_system")
                    ->send();
                return $this->refresh();
            }
            else{
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
            $this->refresh();
        }
        $history_orders = new ActiveDataProvider([
            'query' => Orders::find()->where(['is_user' => Yii::$app->user->identity->id]),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);

        return $this->render('personal-area',compact('history_orders','is_subNews','sub_news_save','order'));
    }


    public function actionAdminPanel(){
        $this->view->title = "Панель админа";
        $subNews = new ActiveDataProvider([
            'query' => SubscriptionNews::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $feedBack = new ActiveDataProvider([
            'query' => FeedBack::find(),
            'pagination' => [
                'pageSize' => 5,
            ],
        ]);
        $orders = new ActiveDataProvider([
            'query' => Orders::find()->with('user'),
            'pagination' => [
                'pagesize' => 10,
            ],
        ]);

        $transactions = new ActiveDataProvider([
            'query' => Transactions::find(),
            'pagination' => [
                'pagesize' => 10,
            ],
        ]);


        return $this->render('admin-panel',compact('subNews','orders', 'transactions', 'feedBack'));
    }
    public function actionDeleteOrders($id){
        $orders = Orders::findOne($id)->delete();
        $this->redirect('/home/admin-panel');
    }
    public function actionDeleteFeedback($id){
        $feedBack = FeedBack::findOne($id)->delete();
        $this->redirect('/home/admin-panel');
    }
    public function actionDeleteSubnews($id){
        $subNews = SubscriptionNews::findOne($id)->delete();
        $this->redirect('/home/admin-panel');
    }
    public function actionDeleteTransactions($id){
        $transactions = Transactions::findOne($id)->delete();
        $this->redirect('/home/admin-panel');
    }
    public function actionAddPosts(){
        $posts = new Posts();
        $users = User::find()->select('email')->all();
        if ($posts->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isPost){
                $posts->image = UploadedFile::getInstance($posts, 'image');
                $posts->date = Yii::$app->formatter->asDate(date('Y-m-d'));
                $posts->image = $posts->upload();
            }
            if($posts->save()){
                Yii::$app->session->setFlash('success', 'Новость сохранена.');
            }
            if (isset($_POST['send_out'])){
                foreach ($users as $user){
                Yii::$app->mailer->compose()
                    ->setTo($user->email)
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('GreenCash '.$posts->title)
                    ->setTextBody("Вы получили это сообщение так как зарегистрированы на GreenCash. "
                        .strip_tags($posts->text))
                    ->send();
                }
                Yii::$app->session->setFlash('success', 'Новость разослана на email пользователей.');
            }
            $this->refresh();
        }else {
            false;
        }

        return $this->render('add-posts', compact('posts'));
    }


    public function actionAddSliders(){
        $sliders = new Sliders();
        if ($sliders->load(Yii::$app->request->post())) {
            if(Yii::$app->request->isPost){
                $sliders->image = UploadedFile::getInstance($sliders, 'image');
                $sliders->image = $sliders->upload();
            }
            $sliders->save();
            $this->goHome();
        }else {
            false;
        }
        return $this->render('add-sliders',compact('sliders'));
    }
    public function actionUpdateSlider($id){
        $sliders = Sliders::find()->where(['id'=>$id])->one();
        if ($sliders->load(Yii::$app->request->post())) {
            $sliders->save();
            $this->redirect('/home/change-posts');
        }
        else{
            false;
        }
        return $this->render('update-slider',compact('sliders'));
    }
    public function actionDeleteSlider($id){
        $sliders = Sliders::findOne($id)->delete();
         $this->redirect('/home/change-posts');
    }



    public function actionChangePosts(){
        $posts = new ActiveDataProvider([
            'query' => Posts::find(),
            'pagination' => [
                'pagesize' => 5,
            ],
        ]);
         $reviews = new ActiveDataProvider([
            'query' => Reviews::find(),
            'pagination' => [
                'pagesize' => 10,
            ],
        ]);
        $sliders = new ActiveDataProvider([
            'query' => Sliders::find(),
            'pagination' => [
                'pagesize' => 10,
            ],
        ]);
        return $this->render('change-posts', compact('posts','reviews','sliders'));
    }

    public function actionAddReviews(){
        $reviews = new Reviews();
        if ($reviews->load(Yii::$app->request->post())){
            $reviews->save();
            $this->redirect('/home/admin-panel');
        }
        return $this->render('add-reviews', compact('reviews'));
    }
     public function actionDeletePosts($id){
        $posts = Posts::findOne($id)->delete();
        $this->redirect('/home/change-posts');

     }
     public function actionDeleteReviews($id){
        $reviews = Reviews::findOne($id)->delete();
        $this->redirect('/home/change-posts');
     }
     public function actionUpdateReviews($id){
        $reviews = Reviews::find()->where(['id'=>$id])->one();
        if ($reviews->load(Yii::$app->request->post())) {
            $reviews->save();
            $this->redirect('/home/change-posts');
        }
        else{
            false;
        }
        return $this->render('update-reviews', compact('reviews'));
     }
    public function actionUpdatePosts($id){
        $posts = Posts::find()->where(['id' => $id])->one();
        if ($posts->load(Yii::$app->request->post())) {
            $posts->image = UploadedFile::getInstance($posts, 'image');
            $posts->image = $posts->upload();
            $posts->save();
            $this->redirect('/home/change-posts');
        }else {
            false;
        }
        return $this->render('update-posts', compact('posts'));

    }

    






}