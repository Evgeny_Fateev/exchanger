<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 21.07.2017
 * Time: 15:16
 */

namespace app\controllers;


use app\models\User;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\db\Expression;
class BehaviorsController extends Controller
{
    public function behaviors(){
        return [
            'access' => [
                'class' => AccessControl::className(),

                'rules' => [
                    [
                        'allow' => true,
                        'controllers' => ['home'],
                        'actions' => ['add-posts', 'change-posts','add-sliders','add-reviews','admin-panel','update-posts','update-reviews',
                        'delete-posts','redaction-text','delete-orders', 'delete-reviews','delete-slider','update-slider',
                        'delete-feedback','delete-subnews','delete-transactions'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->email);
                        }
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['transactions'],
                        'actions' => ['exchang'],
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return User::isUserAdmin(Yii::$app->user->identity->email);
                        }
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['home'],
                        'actions' => ['personal-area'],
                        'roles' => ['@'],
                    ],
                    [
                        'allow' => true,
                        'controllers' => ['page'],

                    ],
                    [
                        'allow' => true,
                        'controllers' => ['transactions'],
                        'actions' => ['result','success','fail','send','wm'],
                        
                    ],

                ],
            ],
            ];

    }
}