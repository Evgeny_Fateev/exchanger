<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 25.07.2017
 * Time: 11:15
 */

namespace app\controllers;

include ('../vendor/autoload.php');

use app\models\Transactions;
use Yii;
use yii\web\Controller;
use baibaratsky\WebMoney\WebMoney;
use baibaratsky\WebMoney\Signer;
use baibaratsky\WebMoney\Request\Requester\CurlRequester;
use baibaratsky\WebMoney\Api\X\X2;

class TransactionsController extends BehaviorsController
{
     
    public function actionExchang(){
        $transactions = new Transactions();
        $last_num_trans = Transactions::find()->select(['number_transaction'])->max('number_transaction');
        if ($transactions->load(Yii::$app->request->post())) {
        $webMoney = new WebMoney(new CurlRequester);
        $request = new X2\Request;
        $request->setSignerWmid($transactions->wmid);//658251617935
        $request->setTransactionExternalId($transactions->number_transaction = $last_num_trans+1); // Unique ID of the transaction in your system
        $request->setPayerPurse($transactions->sender_purse);//R200348960891
        $request->setPayeePurse($transactions->recipient_purse);//Z748494898487
        $request->setAmount($transactions->amount); // Payment amount
        $request->setDescription($transactions->description);

        $request->sign(new Signer('658251617935', 'C:/OpenServer/domains/exchanger.loc/vendor/baibaratsky/nokey/658251617935.kwm', 'cqlmfekq2334529'));

        if ($request->validate()) {
            /** @var X2\Response $response */
            $response = $webMoney->request($request);

            if ($response->getReturnCode() === 0) {
                echo 'Успешный перевод, id транзакции: ' . $response->getTransactionId();
                echo '<br/>';
                echo '<a href="/home/admin-panel" class="btn btn-success">Вернуться в админку</a>';
                $transactions->save();
            } else {
                echo 'Ошибка платежа: ' . $response->getReturnDescription();
            }
        } else {
            echo 'Request errors: ' . PHP_EOL;
            foreach ($request->getErrors() as $error) {
                echo ' - ' . $error . PHP_EOL;
            }
        }
            $this->redirect('https://w3s.webmoney.ru/asp/XMLTrans.asp');
            return $this->refresh();
        }

        return $this->render('exchang',compact( 'transactions'));

     }
     
     public function actionSend(){
        return $this->render('send');
    }
     public function actionResult(){
        return $this->render('result');
    }
    public function actionSuccess(){
        return $this->render('success');
    }
    public function actionFail(){
        return $this->render('fail');
    }
    public function actionWm(){
        return $this->render('wm');
    }


}