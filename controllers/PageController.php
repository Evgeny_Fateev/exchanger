<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 09.08.2017
 * Time: 12:50
 */

namespace app\controllers;


use app\models\Feedback;
use app\models\Orders;
use app\models\Posts;
use app\models\RedactionText;
use app\models\User;
use Codeception\Module\Yii2;
use Yii;
use yii\data\Pagination;
use yii\web\Controller;

class PageController extends BehaviorsController
{
    public $layout = '@app/views/layouts/exchanger.php';

    public function actionReplenish(){
        $replenish = new Orders();
        $text_pm = RedactionText::find()->select('payment_system')->one();
        if($replenish->load(Yii::$app->request->post())) {
            $replenish->type = 'Пополнение';
            $replenish->payment_system = $_POST['pay_system'];
            $user = User::find()->where(['email' => $replenish->email])->select(['id','email'])->one();
            if ($user->email != null)
            {
                $replenish->is_user = $user->id;
            }
            else {
                $replenish->is_user = 0;
            }
            if(!isset($_POST['is_confirm'])){
                Yii::$app->session->setFlash('error', 'Вы не согласились на обработку данных.');
                return $this->refresh();
            }
            if($replenish->save()){
                Yii::$app->session->setFlash('success', 'Спасибо, ваша заявка принята.');
                $replenish->email = \Yii::$app->mailer->compose()
                    ->setTo('zhenja1509@gmail.com')
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Заявка на '. $replenish->type)
                    ->setTextBody("Пользователь $replenish->name,  email: $replenish->email, телефон: $replenish->phone оставил заявку на:
                        $replenish->type,  платежной системой $replenish->payment_system")
                    ->send();
                return $this->refresh();
            }
            else{
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
            $this->refresh();
        }
        return $this->render('replenish',compact('replenish','text_pm'));
    }

    public function actionOutput(){
        $output = new Orders();
        $text_pm = RedactionText::find()->select('payment_system')->one();
        if($output->load(Yii::$app->request->post())) {
            $output->type = 'Вывод';
            $output->payment_system = $_POST['pay_system'];
            $user = User::find()->where(['email' => $output->email])->select(['id','email'])->one();
            if ($user->email != null)
            {
                $output->is_user = $user->id;
            }
            else {
                $output->is_user = 0;
            }
            if(!isset($_POST['is_confirm'])){
                Yii::$app->session->setFlash('error', 'Вы не согласились на обработку данных.');
                return $this->refresh();
            }
            if($output->save()){
                Yii::$app->session->setFlash('success', 'Спасибо, ваша заявка принята.');
                $output->email = \Yii::$app->mailer->compose()
                    ->setTo('zhenja1509@gmail.com')
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Заявка на '.$output->type)
                    ->setTextBody("Пользователь $output->name,  email: $output->email, телефон: $output->phone оставил заявку на: 
                        $output->type,  платежной системой $output->payment_system")
                    ->send();
                return $this->refresh();
            }
        else{
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
            $this->refresh();
        }
        return $this->render('output', compact('output','text_pm'));
    }

    public function actionInformation(){

        return $this->render('information');
    }
    public function actionContacts(){
        $feedback = new Feedback();
        if ($feedback->load(Yii::$app->request->post())) {
            if($feedback->save()){
                $feedback->email = \Yii::$app->mailer->compose()
                    ->setTo('zhenja1509@gmail.com')
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Обратная связь')
                    ->setTextBody("Пользователь $feedback->name,  $feedback->email, телефон: $feedback->telefon оставил обратную связь: $feedback->text")
                    ->send();
                $this->refresh();
            }
        }else {
            false;
        }
        return $this->render('contacts',compact('feedback'));
    }
    public function actionAllPosts(){
        $query = Posts::find();
        $pages = new Pagination(['totalCount' => $query->count(), 'pageSize' => 6, 'forcePageParam' => false, 'pageSizeParam'=>false]);
        $posts = $query->offset($pages->offset)->orderBy(['date' => SORT_DESC])->limit($pages->limit)->all();
        return $this->render('all-posts',compact('posts','pages'));
    }

    public function actionSinglePost($id){
        $blogs = Posts::findOne($id);
        $blogs_prew = Posts::find()->select(['id','title','image'])->where(['<', 'id', $id])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        $blogs_next = Posts::find()->select(['id','title'])->where(['>', 'id', $id])->orderBy(['id' => SORT_DESC])->limit(1)->one();
        $related_blogs = Posts::find()->orderBy(['date' => SORT_DESC])->limit(3)->all();
        return $this->render('single-post', compact('blogs','related_blogs', 'blogs_prew', 'blogs_next'));
    }

}