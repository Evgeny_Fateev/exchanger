<?php

namespace app\controllers;

use app\models\Orders;
use app\models\Posts;
use app\models\RedactionText;
use app\models\SignupForm;
use app\models\Sliders;
use app\models\User;
use app\models\Reviews;
use Yii;
use yii\filters\AccessControl;
use yii\helpers\Html;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;


class SiteController extends Controller
{
    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }



    public function actionIndex()
    {
        $this->layout = '@app/views/layouts/exchanger.php';
        $order = new Orders();
        $text = RedactionText::find()->select('text')->one();
        $sliders = Sliders::find()->all();
        if($order->load(Yii::$app->request->post())) {
            $user = User::find()->where(['email' => $order->email])->select(['id','email'])->one();
            if ($user->email != null)
            {
                $order->is_user = $user->id;
            }
            else {
                $order->is_user = 0;
            }
            if(!isset($_POST['is_confirm'])){
                Yii::$app->session->setFlash('error', 'Вы не согласились на обработку данных.');
                return $this->refresh();
            }
            if($order->save()){
                Yii::$app->session->setFlash('success', 'Спасибо, ваша заявка принята.');
                $order->email = \Yii::$app->mailer->compose()
                    ->setTo('zhenja1509@gmail.com')
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Заявка на '. $order->type)
                    ->setTextBody("Пользователь $order->name,  email: $order->email, телефон: $order->phone оставил заявку на: 
                        $order->type,  платежной системой $order->payment_system")
                    ->send();
                return $this->refresh();
            }
            else{
                Yii::$app->session->setFlash('error', 'Ошибка');
            }
            $this->refresh();
        }
        $posts = Posts::find()->orderBy(['date'=>SORT_DESC])->limit(3)->all();
        $reviews = Reviews::find()->orderBy(['id' => SORT_DESC])->limit(9)->all();
        return $this->render('index', compact('posts','order','reviews','sliders','text'));
    }


    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post())) {
            if ($user = $model->signup()) {
                $url = Yii::$app->urlManager->createAbsoluteUrl(['site/confirm','id'=>$user->id,'key'=>$user->auth_key]);
                $email = \Yii::$app->mailer->compose()
                    ->setTo($user->email)
                    ->setFrom('zhenja94@bk.ru')
                    ->setSubject('Подтверждение E-mail')
                    ->setTextBody("Для подтверждения почты нажмите на ссылку " .$url)
                    ->send();
                if($email){
                    Yii::$app->getSession()->setFlash('success','Письмо для подтверждения отправлено Вам на почту!');
                }
                else{
                    Yii::$app->getSession()->setFlash('warning','Ошибка!');
                }
                return $this->refresh();
            }
        }

        return $this->render('signup', ['model' => $model,]);
    }
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();

        if ($model->load(Yii::$app->request->post()) && $model->loginAdmin()) {
            return $this->goBack();
        }
        return $this->render('login', [
            'model' => $model,
        ]);
    }
    public function actionConfirm($id, $key)
    {
        $model = User::find()->where([
            'id'=>$id,
            'auth_key'=>$key,
            'status'=>0,
        ])->one();
        if(!empty($model)){
            $model->status=10;
            $model->save();
            Yii::$app->getSession()->setFlash('success','E-mail подтвержден!');
        }
        else{
            Yii::$app->getSession()->setFlash('warning','Ошибка!');
        }
        return $this->goHome();
    }
    public  function actionError(){
        $this->layout = '@app/views/layouts/exchanger.php';
        return $this->render('error');
    }
    public function actionAddAdmin() {
        $model = User::find()->where(['username' => 'admin'])->one();
        if (empty($model)) {
            $user = new User();
            $user->username = 'admin';
            $user->email = '123@m.ru';
            $user->setPassword('123');
            $user->generateAuthKey();
            if ($user->save()) {
                echo 'good';
            }
        }
    }
//    public function actionSignup()
//    {
//        $model = new SignupForm();
//
//        if ($model->load(Yii::$app->request->post())) {
//            if ($user = $model->signup()) {
//
//                if (Yii::$app->getUser()->login($user)) {
//                    return $this->goHome();
//                }
//            }
//        }
//
//        return $this->render('signup', [
//            'model' => $model,
//        ]);
//    }




}
