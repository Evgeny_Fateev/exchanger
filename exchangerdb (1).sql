-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Хост: 127.0.0.1:3306
-- Время создания: Окт 18 2017 г., 02:04
-- Версия сервера: 5.6.34
-- Версия PHP: 5.6.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `exchangerdb`
--

-- --------------------------------------------------------

--
-- Структура таблицы `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `email` varchar(50) NOT NULL DEFAULT '0',
  `telefon` varchar(50) NOT NULL DEFAULT '0',
  `title` varchar(50) NOT NULL DEFAULT '0',
  `text` varchar(500) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `feedback`
--

INSERT INTO `feedback` (`id`, `name`, `email`, `telefon`, `title`, `text`) VALUES
(3, 'Zhenja', 'zhenja1509@gmail.com', '0', '0', 'Help'),
(4, 'Zhenja', 'zhenja1509@gmail.com', '0', '0', 'Help'),
(5, 'Петя', 'fvd@v.ru', '0', '0', 'ADMin help me!'),
(6, 'Вася', 'zdf@3.ru', '323232', 'Help', 'fdbfgbgf');

-- --------------------------------------------------------

--
-- Структура таблицы `migration`
--

CREATE TABLE `migration` (
  `version` varchar(180) NOT NULL,
  `apply_time` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `migration`
--

INSERT INTO `migration` (`version`, `apply_time`) VALUES
('m000000_000000_base', 1501219617),
('m170728_052525_create_user_table', 1501219622);

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL DEFAULT '0',
  `email` varchar(255) NOT NULL DEFAULT '0',
  `phone` varchar(255) NOT NULL DEFAULT '0',
  `type` varchar(255) NOT NULL DEFAULT '0',
  `payment_system` varchar(500) NOT NULL DEFAULT '0',
  `is_user` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `name`, `email`, `phone`, `type`, `payment_system`, `is_user`) VALUES
(1, 'Вася', 'fjvfnd@c.ru', '39393302', 'Пополнение', '2', 0),
(2, 'Nfvfm', 'fbbr@c.ru', '33232', 'Пополнение', '4', 0),
(3, 'fdbdf', 'dbdb@b.ru', '222', 'Пополнение', 'Яндекс деньги', 0),
(4, 'Zehnja', '123@m.ru', '3232', 'Вывод', 'Киви', 22),
(5, 'Петя', 'zhenja94@bk.ru', '2121212121', 'Вывод', 'Тинькофф', 23),
(6, 'Вася', 'zhenja1509@gmail.com', '212121', 'Вывод', 'Альфа банк', 21),
(7, 'fdb', 'zhenja94@bk.ru', '3232', 'Вывод', 'Перфект Мани', 23),
(8, 'dsger', 'zhenja94@bk.ru', '3232', 'Пополнение', 'Биткоин', 23),
(10, 'verbr', '123@m.ru', '3232', 'Пополнение', 'Вебмани', 22),
(12, 'fvd', 'dvfvf@c.ru', '232', 'Пополнение', 'dddfd', 0),
(13, 'Петя', 'vjfbfr@v.tu', '9943493', 'Пополнение', 'Тинькофф', 22),
(15, 'Вася', 'dfbfb@b.ru', '32323', 'Пополнение', 'Тинькофф', 0),
(17, 'Zehnja', 'zhenja1509@gmail.com', 'dvd', 'Пополнение', 'Биткоин', 21),
(18, 'vfd', 'zhenja1509@gmail.com', 'fvf', 'Вывод', 'Альфа банк', 21),
(19, 'vdfvd', 'zhenja94@bk.ru', 'dfv', 'Вывод', '0', 23),
(20, 'Zehnja', 'zhenja94@bk.ru', 'fdvfd', 'Вывод', 'Перфект Мани', 23),
(21, 'Zehnja', 'zhenja1509@gmail.com', 'fvfvf', 'Пополнение', 'Тинькофф', 21),
(22, 'Zehnja', 'zhenja1509@gmail.com', 'dvdv', 'Пополнение', 'Яндекс деньги', 21);

-- --------------------------------------------------------

--
-- Структура таблицы `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) NOT NULL DEFAULT '0',
  `image` varchar(255) NOT NULL DEFAULT '0',
  `text` varchar(1000) NOT NULL DEFAULT '0',
  `date` date NOT NULL,
  `category` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `posts`
--

INSERT INTO `posts` (`id`, `title`, `image`, `text`, `date`, `category`) VALUES
(19, 'Новость', 'images_home/c69f865dc4b71dc2efaee7c52313dddd.jpg', '<p>СВарщик</p>\r\n<p><img src=\"/images_posts/1.jpg\" alt=\"\" width=\"372\" height=\"292\" /></p>', '2017-08-09', 'Блог'),
(20, 'Шок', 'images_home/98085b833e24bc99a396a26c380cabb3.jpg', '<p>Это должно было случиться сегодня просто, не тот день для того чтобы выяснять кто прав, а кто виноват. Всему все свое время</p>\r\n<p><img src=\"/images_posts/images%20(4).jpg\" alt=\"\" width=\"263\" height=\"191\" /></p>', '2017-08-09', 'Новость'),
(21, 'Новость223232', 'images_home/0ba42cb321d24d7e0a9d87a8f2e81a66.jpg', '<p>Все этого не ждали, но это свершилось</p>', '2017-08-07', 'Блог'),
(23, 'ШОК!!!!!', 'images_home/3831680b1b1ef74724352c7474106f45.jpg', '<p>Снижаем цены в сто раз, но это не точно, мы же не будем так делать. А сегодня в деревне произошл случай, который повлек за собой необъяснимывае вещи, которые способствуют развитию бурной промышленности в селе.<img src=\"/images_posts/images11.jpg\" alt=\"\" width=\"312\" height=\"161\" /></p>', '2017-08-09', 'Блог'),
(26, 'Вы хотите это услышать', 'images_home/bd2c5bdbf56118d9ed84f54717b37dc5.jpg', '<p>Но это не хочет услышать вас</p>\r\n<p><img src=\"/images_posts/images%20(4).jpg\" alt=\"\" width=\"263\" height=\"191\" /></p>\r\n<p><img src=\"/images_posts/1.jpg\" alt=\"\" width=\"372\" height=\"292\" /></p>', '2017-08-09', 'Блог'),
(27, 'Скидка 15%', 'images_home/63e6804feb9ea180f3e620dfd8c68237.jpg', '<p>Распродаем все, спешите купить</p>', '2017-08-31', 'Новость'),
(28, 'Скидка 11', 'images_home/8109d117dba5411a98bb62cbba164447.jpg', '<p>&nbsp;Это да</p>\r\n<p><img src=\"/images_posts/3.jpg\" alt=\"\" width=\"350\" height=\"330\" /></p>', '2017-08-31', 'Новость'),
(29, '1011', 'images_home/f70e75f9a50118355392e6a5ff464042.jpg', '<p>аллап апипа</p>\r\n<p><img src=\"/images_posts/images11.jpg\" alt=\"\" width=\"312\" height=\"161\" /></p>', '2017-08-31', 'Новость'),
(30, 'авм', 'images_home/9d1c378e7d3e9872901f47470fc68625.jpg', '<p>папапа</p>', '2017-08-31', 'Блог');

-- --------------------------------------------------------

--
-- Структура таблицы `redaction_text`
--

CREATE TABLE `redaction_text` (
  `id` int(11) NOT NULL,
  `text` varchar(10000) NOT NULL DEFAULT '0',
  `payment_system` varchar(10000) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `redaction_text`
--

INSERT INTO `redaction_text` (`id`, `text`, `payment_system`) VALUES
(1, '<p>Бизнес в интернете развивается бешеными темпами. В сети предлагается огромное количество товаров и услуг &ndash; от косметики до обучения иностранных языков. Работая в электронной сети, без электронных денег - не обойтись. Деньги хранятся на счетах той или иной электронной системы, и периодически возникает необходимость обменивать электронные деньги на те, которые легко обналичивать или же наоборот &ndash; тратить в Сети.<br />Соответственно возрастает и потребность в онлайн-обменнике валют Электронную валюту, как и любую другую кроме того, чтобы тратить, нужно так же и выгодно обменять. В связи с этим возникает большое количество онлайн-сервисов для обмена электронных денег. Это сервисы &ndash; очень удобные.</p>', 'Вебмани,Биткоин,Перфект Мани,Киви,Яндекс деньги,Сбербанк,Альфа банк,Тинькофф');

-- --------------------------------------------------------

--
-- Структура таблицы `reviews`
--

CREATE TABLE `reviews` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL DEFAULT '0',
  `position` varchar(255) NOT NULL DEFAULT '0',
  `text` varchar(500) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `reviews`
--

INSERT INTO `reviews` (`id`, `name`, `position`, `text`) VALUES
(2, 'Петя111', 'Гастрарбайтер', 'Этот сайт лучший в мире'),
(3, 'Вася', 'Фитнес-тренер', 'Такого совершенства  я не видел, это лучшее что нам дано'),
(5, 'Жора', 'Грузчик', 'Когда я пришел на этот сайт я наше себя, ничего лучшего я не видел'),
(10, 'Василий', 'Тракторист', 'Самый лучший сайт'),
(11, 'Аркадий', 'Программист', 'kfddfb');

-- --------------------------------------------------------

--
-- Структура таблицы `sliders`
--

CREATE TABLE `sliders` (
  `id` int(11) NOT NULL,
  `image` varchar(255) NOT NULL DEFAULT '0',
  `text` varchar(500) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `sliders`
--

INSERT INTO `sliders` (`id`, `image`, `text`) VALUES
(1, 'images_home/55c85fb41b0bb6249f30c9c0915ad3c4.jpg', '<p>Все супер конечно</p>\r\n<p>Это было нечто</p>'),
(2, 'images_home/c528fd0d7f1d4c7439131d1a7572d147.jpg', '<p>Снижаем все</p>\r\n<p>Успейте купить</p>');

-- --------------------------------------------------------

--
-- Структура таблицы `subscription_news`
--

CREATE TABLE `subscription_news` (
  `id` int(11) NOT NULL,
  `email` varchar(250) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `subscription_news`
--

INSERT INTO `subscription_news` (`id`, `email`) VALUES
(15, 'vfvd@b.ru'),
(16, 'dcd@vfv.ru'),
(18, 'svdbf@bk.ru'),
(19, 'fdbnmjm@b.ru'),
(20, 'zdv@b.ru'),
(21, 'zhenja@f.ru'),
(22, 'zherdff@b.ru'),
(28, '123@m.ru');

-- --------------------------------------------------------

--
-- Структура таблицы `transactions`
--

CREATE TABLE `transactions` (
  `id` int(11) NOT NULL,
  `wmid` varchar(50) NOT NULL DEFAULT '0',
  `sender_purse` varchar(50) NOT NULL DEFAULT '0',
  `recipient_purse` varchar(50) NOT NULL DEFAULT '0',
  `number_transaction` int(11) NOT NULL DEFAULT '0',
  `amount` varchar(50) NOT NULL DEFAULT '0',
  `description` varchar(300) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `transactions`
--

INSERT INTO `transactions` (`id`, `wmid`, `sender_purse`, `recipient_purse`, `number_transaction`, `amount`, `description`) VALUES
(1, '658251617935', 'R200348960891', 'R104947045218', 3, '0,01', 'Тест тестов'),
(3, '658251617935', 'R200348960891', 'R104947045218', 3, '0.01', 'Тест тестов'),
(4, '658251617935', 'R200348960891', 'R104947045218', 4, '0.01', 'Тест тестов'),
(6, '658251617935', 'R200348960891', 'R104947045218', 6, '0.01', 'Тест тестов 2.0'),
(12, '658251617935', 'R200348960891', 'R104947045218', 10, '0.01', '333'),
(14, '658251617935', 'R200348960891', 'R104947045218', 12, '0.01', 'vfvgb'),
(15, '658251617935', 'R200348960891', 'R104947045218', 13, '0.01', '111122'),
(17, '658251617935', 'R200348960891', 'R104947045218', 14, '0.01', '228'),
(19, '658251617935', 'R200348960891', 'R104947045218', 16, '0.01', 'ghnhgm'),
(20, '658251617935', 'R200348960891', 'R104947045218', 2, '0.01', 'Test_payment'),
(21, '658251617935', 'R200348960891', 'R104947045218', 18, '0.01', 'fdv'),
(22, '658251617935', 'R200348960891', 'R104947045218', 19, '0.01', 'fdv'),
(23, '658251617935', 'R200348960891', 'R104947045218', 20, '0.01', 'fdv11'),
(24, '658251617935', 'R200348960891', 'R104947045218', 21, '0.01', 'fdv11'),
(25, '658251617935', 'R200348960891', 'R104947045218', 22, '0.01', 'gfbfg'),
(26, '658251617935', 'R200348960891', 'R104947045218', 23, '0.01', 'gfbf');

-- --------------------------------------------------------

--
-- Структура таблицы `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `auth_key` varchar(32) COLLATE utf8_unicode_ci NOT NULL,
  `password_hash` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password_reset_token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` smallint(6) NOT NULL DEFAULT '10',
  `created_at` int(11) NOT NULL,
  `updated_at` int(11) NOT NULL,
  `is_admin` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `user`
--

INSERT INTO `user` (`id`, `username`, `auth_key`, `password_hash`, `password_reset_token`, `email`, `status`, `created_at`, `updated_at`, `is_admin`) VALUES
(21, '', 'Z7KhefPvgMeActqnXoOIlQiSbR2dyCBl', '$2y$13$hoUhWEy27805cGWqHWiS8.udqc76OAmjm2FTfIuC2gdm/tnGKtnqi', NULL, 'zhenja1509@gmail.com', 10, 1501233546, 1501233608, 0),
(22, 'admin', 'E9XJhaTN2s4NgDowe1xc8TxT_IgvYGuU', '$2y$13$a/VTFNufmwAJoXkWF2qM3u0DYs2R9p8GV4hy5ZzqEkfrCPNECv7dS', NULL, '123@m.ru', 10, 1502273930, 1502273930, 1),
(23, 'Женька', 'YpyXvGagoGzPyRT-ye5fSPVM6GkyR9fe', '$2y$13$Sa7AlDBjsYEf6QbDx/hJb.Z2pH6X5WO/434Bq2D6udd.ULVYFqKDa', NULL, 'zhenja94@bk.ru', 10, 1502274305, 1502274347, 0);

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `migration`
--
ALTER TABLE `migration`
  ADD PRIMARY KEY (`version`);

--
-- Индексы таблицы `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `redaction_text`
--
ALTER TABLE `redaction_text`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `subscription_news`
--
ALTER TABLE `subscription_news`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `transactions`
--
ALTER TABLE `transactions`
  ADD PRIMARY KEY (`id`);

--
-- Индексы таблицы `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `password_reset_token` (`password_reset_token`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT для таблицы `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT для таблицы `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT для таблицы `redaction_text`
--
ALTER TABLE `redaction_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT для таблицы `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT для таблицы `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT для таблицы `subscription_news`
--
ALTER TABLE `subscription_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
--
-- AUTO_INCREMENT для таблицы `transactions`
--
ALTER TABLE `transactions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT для таблицы `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
