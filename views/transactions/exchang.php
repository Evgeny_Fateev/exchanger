<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 26.07.2017
 * Time: 17:49
 */

use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?$form = ActiveForm::begin();?>
    <?= $form->field($transactions, 'wmid')->textInput();?>
    <?= $form->field($transactions, 'sender_purse')->textInput();?>
    <?= $form->field($transactions, 'recipient_purse')->textInput();?>
    <?= $form->field($transactions, 'description')->textInput();?>
    <?= $form->field($transactions, 'amount')->textInput();?>
    <?= Html::submitButton('Отправить', ['class'=>'btn btn-success']) ?>
<? ActiveForm::end();?>

