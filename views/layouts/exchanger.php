<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;
use yii\widgets\ActiveForm;
use app\widgets\SubNewsWidget;
AppAsset::register($this);
?>
<?php $this->beginPage() ?>


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <?php if(Yii::$app->language == 'ru'):  ?>
        <title>Обменник Webmoney (вебмани) на наличные в Москве. Удобные способы пополнения Webmoney</title>
    <?php endif;?>
    <?php if(Yii::$app->language == 'en'):  ?>
        <title>"Green Cash" - profitable e-money exchange</title>
    <?php endif;?>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Bootstrap -->

    <!-- GOOGLE FONT -->
    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>


<div class="outslider_loading">
    <div class="outslider_loader"></div>
</div>
<header class="header-area navbar-fixed-top">
    <div class="container custom-header">
        <div class="row">
            <div id="menuzord" class="menuzord">
                <a href="/" class="menuzord-brand"><img src="/web/images/logo.png" alt=""></a>


                <ul class="menuzord-menu menuzord-menu-bg">
                    <li><a href="/"><?=Yii::t('common','Главная')?></a></li>
                    <li><a href="/page/replenish"><?=Yii::t('common','Пополнить')?></a></li>
                    <li><a href="/page/output"><?=Yii::t('common','Вывести')?></a></li>
                    <li><a href="/page/all-posts"><?=Yii::t('common','Блоги')?></a>
                    <li><a href="http://wiki.webmoney.ru/projects/webmoney/wiki/Условия_для_обменных_пунктов"><?=Yii::t('common','Информация')?></a>

                         <!--<ul class="dropdown">
                            <li><a ">Информация</a></li>
                           <li><a href="shop-product.html">Правила системы</a></li>
                            <li><a href="shop-single-product.html">Что такое WebMoney</a></li>
                            <li><a href="shoping-cart.html">Уведомление о рисках</a></li>
                            <li><a href="shoping-cart.html">Получение аттестатов</a></li>
                            <li><a href="shoping-cart.html">Соглашение с WebMoney</a></li>
                        </ul>-->

                    </li>
                    <li><a href="/page/contacts"><?=Yii::t('common','Контакты')?></a></li>

                    <li><?= Html::a('en', ['/', 'language' => 'en']) ?></li>

                    <li><?= Html::a('ru', ['/', 'language' => 'ru']) ?></li>
                </ul>


                <div class="appointment-area">

                    <p><a href="/home/personal-area"><?=Yii::t('common','Личный кабинет')?></a></p>
                </div>
            </div>
        </div>
    </div>
</header>
<?= $content ?>
<?php $this->endBody() ?>

<footer class="footer-area">
    <div class="footer-top-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-md-3 no-padding">
                    <div class="footer-top-single text-center">
                        <p><?=Yii::t('common','Наши контакты')?></p>
                        <p>тел. +7 (913) 155-09-18</p>

                    </div>
                </div>
                <div class="col-sm-3 col-md-3 no-padding">
                    <div class="footer-top-single footer-top-single-bg-2 text-center">
                        <p>E-mail : GreenCashwm@gmail.com</p>
                        <p>Skype : GreenCashwm</p>
                        <p>Telegram : @GreenCash_wm</p>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 no-padding">
                    <div class="footer-top-single footer-top-single-bg-2 text-center">
                        <p>WMID: 658251617935</p>
                        <p>R200348960891</p>
                        <p>Z748494898487</p>
                    </div>
                </div>
                <div class="col-sm-3 col-md-3 no-padding">
                    <div class="footer-top-single text-center">
                        <p><?=Yii::t('common','г.Москва')?></p>
                        <p><?=Yii::t('common','ул. Орджоникидзе, д.11, стр.2')?></p>

                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="footer-main-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="footer-main-single footer-first-content clearfix">
                        <h2><?=Yii::t('common','Режим работы')?></h2>
                        <p>


                        </p>
                        <div class="footer-time">
                            <ul>
                                <li><?=Yii::t('common','С Пн. по Пт.')?></li>
                                <li>8:00 - 16:00</li>
                                <li><?=Yii::t('common','Суббота')?></li>
                                <li>8:00 - 16:00</li>
                                <li><?=Yii::t('common','Воскресенье')?></li>
                                <li><?=Yii::t('common','закрыто')?></li>
                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="footer-main-single clearfix">
                        <h2><?=Yii::t('common','Услуги')?></h2>
                        <div class="footer-service">
                            <ul>
                                <li><a href="/page/replenish"><?=Yii::t('common','Пополнить')?></a></li>
                                <li><a href="/page/output"><?=Yii::t('common','Вывести')?></a></li>
                                <!--<li><a href="#">Для юр. лиц</a></li>-->
                                <li><a href="#">Тарифы</a></li>

                            </ul>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="footer-main-single footer-third-content clearfix">
                        <h2><?=Yii::t('common','Курсы валют')?></h2>
                        <img src="http://webmoneyexchange.biz/informer/show.php?informer=88x31&cur1=WMR&cur2=WMZ&color=green" height="40" width="120"  alt="Обмен WebMoney" title="Курсы WebMoney WMR-WMZ" border="0" /><br/>
                        <img src="http://webmoneyexchange.biz/informer/show.php?informer=88x31&cur1=WMR&cur2=WME&color=green" height="40" width="120"  alt="Обмен WebMoney" title="Курсы WebMoney WMR-WME" border="1" />
                    </div>
                </div>
                <div class="col-sm-6 col-md-2">
                    <div class="footer-main-single footer-third-content clearfix">
                        <h2><?=Yii::t('common','Аттестован webmoney')?></h2>
                        <a href="https://www.megastock.com/" target="_blank"><img src="https://www.webmoney.ru/img/icons/88x31_wm_blue.png" alt="www.megastock.com" border="0"/></a>
                        <a href="https://passport.webmoney.ru/asp/certview.asp?wmid=658251617935" target="_blank">
                            <img src="https://www.webmoney.ru/img/icons/88x31_wm_blue.png" alt="Здесь находится аттестат нашего WM идентификатора 658251617935" border="0" /><br />
                            <span style="font-size: 0,7em;">Проверить аттестат</span></a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="footer-main-single footer-first-content f-fourth-content clearfix">
                        <h2><?=Yii::t('common','Подписаться на новости')?></h2>
                        <p><?=Yii::t('common','Не пропустите важную информацию о электронных деньгах')?>.</p>
                        <?= SubNewsWidget::widget();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom-content-details">
        <div class="container">
            <div class="row">
                <div class="footer-bottom-content">
                    <div class="col-sm-4 col-md-4 no-padding-left">
                        <div class="footer-bottom-single">
                            <p> © 2017 <?=Yii::t('common','Разработка сайта')?> <a href="http://arkell.ruf">IT - studio Arkell</a></p>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4 no-padding">
                        <div class="footer-bottom-single">
                            <div class="footer-menu">
                                <a href="#">О компании</a>
                                <a href="#">Политика конфиденциальности</a>
                                <a href="/page/contacts"><?=Yii::t('common','Контакты')?></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-4 col-md-4  no-padding-right">
                        <div class="footer-bottom-single">
                            <ul class="footer-social text-right">
                                <li style="top:30px"> <a href="#" ><i class="fa fa-facebook" ></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                                <li><a href="#"><i class="fa fa-pinterest"></i></a></li>
                                <li><a href="#"> <i class="fa fa-rss"></i></a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>




</body>

<?php $this->endPage() ?>
