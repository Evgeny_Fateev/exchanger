

<?php
use yii\helpers\Html;
use yii\widgets\ActiveField;
use yii\widgets\ActiveForm;

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Experts</title>


    <![endif]-->
</head>
<body>


<div class="page-title-area blog-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2><?=Yii::t('common','Отдельная статья')?></h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/"><?=Yii::t('common','Главная')?></a></li>
                        <li><a href="/page/all-posts"><?=Yii::t('common','Блоги')?></a></li>
                        <li><a href="#"><?=Yii::t('common','Отдельная статья')?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

<section class="blog-area blog-page-area bg-type-2">
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">

                <div class="single-blog-content-area">
                    <div class="blog-content-single blog-content-single-no-btm-padding">
                        <div class="blog-img">
                            <? echo Html::img('@web/'.$blogs->image)?>
                            <i class="fa fa-image"></i>
                        </div>
                        <div class="blog-text">
                            <ul>
                                <li><?=$blogs->date ?> </li>
                                <li>
                                    <a href="#"><?=$blogs->category?></a>

                            </ul>
                            <h2><?=$blogs->title?></h2>
                            <p><?=$blogs->text ?></p>


                        </div>
                    </div>
                </div>

                <div class="blog-prev-next clearfix">
                    <div class="blog-prev">
                        <div class="blog-prev-content">
                            <p><?=Yii::t('common','Предыдущая статья')?></p>
                            <?php if ($blogs_prew->id != null):?>
                                <h3><a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $blogs_prew->id])?>"><?=$blogs_prew->title?></a></h3>
                            <?php endif;?>


                            <div class="prev-left">
                                <a href="#"><img src="/web/images/blog-siderbar-author-1.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                    <div class="blog-next text-right">
                        <div class="blog-next-content">
                            <p><?=Yii::t('common','Следующая статья')?></p>
                            <?php if ($blogs_next->id != null):?>
                                <h3><a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $blogs_next->id])?>"><?=$blogs_next->title?></a></h3>
                            <?php endif;?>
                            <div class="next-right">
                                <a href="#"><img src="/web/images/blog-siderbar-author-1.png" alt=""></a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="blog-siderbar-area single-blog-thumbnail single-blog-thumbnail-c-padding clearfix">
                    <h2><?=Yii::t('common','Похожии статьи')?></h2>
                    <?php foreach ($related_blogs as $related_blog):?>
                        <div class="col-sm-12 col-sm-4 col-md-4 ">
                            <div class="blog-content-single blog-content-single-no-m-btm">
                                <div class="blog-img" style="width: 209px; height: 167px;">
                                    <? echo Html::img('@web/'.$related_blog->image, ['style' =>'max-width:100%;max-height: 100%'])?>
                                    <i class="fa fa-image"></i>
                                </div>
                                <div class="blog-text">
                                    <ul>
                                        <li><?=$related_blog->date?> </li>
                                    </ul>
                                    <h2><a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $related_blog->id])?>"><?=$related_blog->title?></a></h2>
                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>


                </div>

            </div>
        </div>
</section>


</body>
</html>

