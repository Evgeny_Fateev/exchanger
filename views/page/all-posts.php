<?php

use app\models\User;
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\LinkPager;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Experts</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Bootstrap -->

</head>
<body>

<div class="page-title-area blog-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2><?=Yii::t('common','Блоги')?></h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/"><?=Yii::t('common','Главная')?></a></li>
                        <li><a href="/page/all-posts"><?=Yii::t('common','Блоги')?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->

<!-- =========================
  START BLOG SECTION
============================== -->
<section class="blog-area blog-page-area bg-type-2">
    <div class="blog-content-area">
        <div class="container">
            <div class="row">

                <?php foreach ($posts as $post):?>
                <div class="col-sm-6 col-md-4">
                    <div class="blog-content-single">
                        <div class="blog-img" style="width: 300px; height: 240px;">

                            <? echo Html::img('@web/'.$post->image, ['style' =>'max-width:100%;max-height: 100%;border: 1px solid black'])?>
                            <i class="fa fa-image"></i>
                        </div>
                        <div class="blog-text" style="width: auto; height: 200px;">
                            <ul>
                                <li><?=$post->date ?> </li>
                                <li>
                                    <a href="#"><?=$post->category?></a>

                                </li>
                            </ul>
                            <h2><a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $post->id])?>"><?=$post->title?></a></h2>
                            <p><?=StringHelper::truncate(preg_replace("/<img[^<]+/u", '',$post->text), 150, '...')?></p>
                            <a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $post->id])?>"><i class="fa fa-long-arrow-right"></i> <span><?=Yii::t('common','Подробнее')?></span></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>
        </div>
    </div>

            <?php echo '<div class="text-center">' .LinkPager::widget(['pagination' => $pages]). '</div>';?>
</section>
<!--        <div class="container">-->
<!--            <div class="row">-->
<!--                <div class="col-md-12 expert-pagination blog-pagination text-center clearfix">-->
<!--                    <nav>-->
<!--                        <ul class="pagination">-->
<!--                            <li><a href="#">1</a></li>-->
<!--                            <li><a href="#">2</a></li>-->
<!--                            <li>-->
<!--                                <a href="#" aria-label="Next">-->
<!--                                    <i class="fa fa-long-arrow-right"></i>-->
<!--                                </a>-->
<!--                            </li>-->
<!--                        </ul>-->
<!--                    </nav>-->
<!--                </div>-->
<!--            </div>-->
<!--        </div>-->


</body>
</html>
