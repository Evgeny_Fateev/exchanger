<?php

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Experts</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- Bootstrap -->

</head>
<body>


<div class="page-title-area about-area-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2>About Us</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="#">Home</a></li>
                        <li><a href="#">Pages</a></li>
                        <li><a href="#">About Us</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->


<!-- =========================
  START TEAM SECTION
============================== -->
<section class="team-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3>Experts</h3>
                        <h2>Our Team</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="team-area-details">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-1.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Ahmed Abd-Alhaleem</h2>
                                <p>Head of Investment</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Omar Elnagar</h3>
                            <p>Civil Engineer</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-2.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Ahmed Hassan</h2>
                                <p>Tax Advice</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Ahmed Hassan</h3>
                            <p>Tax Advice</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-3.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Mohamed Habaza</h2>
                                <p>Business Planner</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Mohamed Habaza</h3>
                            <p>Business Planner</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-md-3">
                    <div class="team-single">
                        <div class="team-image">
                            <img src="/web/images/team/team-4.png"  class="img-responsive" alt="">
                            <div class="team-hover-text">
                                <h2>Amr Gamal Sadeq</h2>
                                <p>Stock Market Broker</p>
                            </div>
                        </div>
                        <div class="team-text text-center">
                            <h3>Amr Gamal Sadeq</h3>
                            <p>Stock Market Broker</p>
                            <div class="team-social">
                                <a class="fa fa-facebook" href="#"></a>
                                <a class="fa fa-twitter" href="#"></a>
                                <a class="fa fa-linkedin" href="#"></a>
                                <a class="fa fa-rss" href="#"></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog-explore text-center">
                    <a href="#"><i class="fa fa-long-arrow-right"></i>Meet The Team</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END TEAM SECTION
============================== -->

<!-- =========================
  START ACCORDION 2 SECTION
============================== -->
<section class="accordion-area bg-type-2">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3>All about us!</h3>
                        <h2>About Experts</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="accordion-content">
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="accordion-content-inner">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                            About Experts!
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        With any financial product that you buy, it is important that you know you are getting the best advice from a reputable company as often you will have to provide sensitive information online or over the internet.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            Our Mission!
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        With any financial product that you buy, it is important that you know you are getting the best advice from a reputable company as often you will have to provide sensitive information online or over the internet.
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            Our Goals!
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        With any financial product that you buy, it is important that you know you are getting the best advice from a reputable company as often you will have to provide sensitive information online or over the internet.
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div id="quote-4-slider" class="owl-carousel all-carousel owl-theme">
                        <div class="quote-single">
                            <div class="quote-main-content">
                                <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                                <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                                <p>My work has been greatly helped by the excellent work from Experts team, their advice and support has been first class, I can’t thank them enough for the awesome progress. That’s why we have developed close working relationships with a number of strategic partners who can bring additional skills to the table to complement our own, carefully selected as leaders in their respective fields whose outlook and approach to client care mirror our own high standards. It’s all part our commitment to provide clients with a completed support ! </p>
                            </div>
                            <div class="quote-author">
                                <img src="/web/images/quote-author-4.jpg" alt="">
                                <div class="quote-author-details">
                                    <h3>Mahmoud Baghagho</h3>
                                    <p>Art Director</p>
                                </div>
                            </div>
                        </div>
                        <div class="quote-single">
                            <div class="quote-main-content">
                                <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                                <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                                <p>My work has been greatly helped by the excellent work from Experts team, their advice and support has been first class, I can’t thank them enough for the awesome progress. That’s why we have developed close working relationships with a number of strategic partners who can bring additional skills to the table to complement our own, carefully selected as leaders in their respective fields whose outlook and approach to client care mirror our own high standards. It’s all part our commitment to provide clients with a completed support ! </p>
                            </div>
                            <div class="quote-author">
                                <img src="/web/images/quote-author-2.jpg" alt="">
                                <div class="quote-author-details">
                                    <h3>James Norway</h3>
                                    <p>Art Director</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END ACCORDION 2 SECTION
============================== -->

<!-- =========================
  START SERVICE SECTION
============================== -->
<section class="service-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3>Work, fun & more</h3>
                        <h2>Our Portfolio</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="service-content-area">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12 no-padding">
                    <div id="service-2-slider" class="owl-carousel all-carousel owl-theme">
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-1.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-2.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-3.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-1.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-2.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                        <div class="service-content-single">
                            <div class="service-content-img type-3-service-content-img">
                                <img src="/web/images/service-3.png" alt="">
                                <div class="service-content-img-layer"></div>
                                <div class="service-content-img-layer-icon service-3-content-img-layer-icon"><a href="#"><img src="/web/images/zoom-icon.png" alt=""></a></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog-explore explore-more-2 text-center">
                    <a href="#"><i class="fa fa-long-arrow-right"></i>View Our Gallery</a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END SERVICE SECTION
============================== -->

<!-- =========================
  START COUNTER UP SECTION
============================== -->
<section class="counter-up-area home-6-counter-up-area">
    <div class="container">
        <div class="row">
            <div class="counter-up-content">
                <div class="col-sm-3">
                    <div class="counter-up-content-inner">
                        <h2><span class="counter">5000</span></h2>
                        <p>Million Dollars Saved</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="counter-up-content-inner">
                        <h2><span class="counter">1325</span></h2>
                        <p>Successful Deals</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="counter-up-content-inner">
                        <h2><span class="counter">321</span></h2>
                        <p>Advisors & Experts</p>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="counter-up-content-inner w-r-l-border">
                        <h2><span class="counter">2314</span></h2>
                        <p>Happy Customers</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END COUNTER UP SECTION
============================== -->

<!-- =========================
  START BLOG SECTION
============================== -->
<section class="blog-area bg-type-2">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3>News, tips & more!</h3>
                        <h2>Money Blog</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="blog-content-area">
        <div class="container">
            <div class="row">
                <div class="col-md-4 col-sm-4">
                    <div class="blog-content-single">
                        <div class="blog-img">
                            <img src="/web/images/blog/blog-1.png" alt="" class="img-responsive">
                            <i class="fa fa-image"></i>
                        </div>
                        <div class="blog-text">
                            <ul>
                                <li>Feb 22, 2016 </li>
                                <li>
                                    <a href="#">money,</a>
                                    <a href="#">Tips</a>
                                </li>
                            </ul>
                            <h2><a href="#">Four ways to cheer yourself up on Blue Monday!</a></h2>
                            <p>The third Monday of January is supposed to be the most depressing day of the year. Whether you believe that or not, the long nights, cold weather and trying to keep to new year...</p>
                            <a href="#"><i class="fa fa-long-arrow-right"></i> <span>Read More</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="blog-content-single">
                        <div class="blog-img">
                            <img src="/web/images/blog/blog-2.png" alt="" class="img-responsive">
                            <i class="fa fa-briefcase"></i>
                        </div>
                        <div class="blog-text">
                            <ul>
                                <li>Feb 21, 2016 </li>
                                <li>
                                    <a href="#">Budget, </a>
                                    <a href="#">Survive</a>
                                </li>
                            </ul>
                            <h2><a href="#">The hidden habits that cost you money?</a></h2>
                            <p>Did you know the average adult spends more than £72 a month without really knowing what on? That’s cash that could come in really handy when the credit card bills blow in at the end...</p>
                            <a href="#"><i class="fa fa-long-arrow-right"></i> <span>Read More</span></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-4">
                    <div class="blog-content-single">
                        <div class="blog-img">
                            <img src="/web/images/blog/blog-3.png" alt="" class="img-responsive">
                            <i class="fa fa-vimeo-square"></i>
                        </div>
                        <div class="blog-text">
                            <ul>
                                <li>Feb 22, 2016 </li>
                                <li>
                                    <a href="#">News, </a>
                                    <a href="#">Stories</a>
                                </li>
                            </ul>
                            <h2><a href="#">In the news: this week’s top money stories</a></h2>
                            <p>The weather has taken a turn for the worse and January pay day still seems far away. But you don’t have to venture outdoors or spend any money today. Sit back, relax and catch up...</p>
                            <a href="#"><i class="fa fa-long-arrow-right"></i> <span>Read More</span></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="blog-explore text-center">
                    <a href="#"><i class="fa fa-long-arrow-right"></i> Explore More </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END BLOG SECTION
============================== -->


<!-- =========================
  START QUOTE SECTION
============================== -->
<section class="client-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3>Awesome Customers</h3>
                        <h2>Our Partners</h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="client-slider" class="owl-carousel all-carousel owl-theme">
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-1.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-5.png" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-2.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-6.png" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-3.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-7.png" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-4.png" alt=""></span></a>
                        </div>
                        <div class="client-img">
                            <a href="#"><span><img src="/web/images/client/client-8.png" alt=""></span></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END QUOTE SECTION
============================== -->

<!-- =========================
  START SIGN UP SECTION
============================== -->
<section class="sign-up-area parallax-window" data-parallax="scroll" data-image-src="./images/sign-up-bg.png">
    <div class="container">
        <div class="row">
            <div class="sign-up-content">
                <div class="col-sm-9">
                    <div class="sign-up-left">
                        <span>Worried about debt?</span>
                        <span> Find out where to get free help now!</span>
                    </div>
                </div>
                <div class="col-sm-3">
                    <div class="sign-up-btn">
                        <a href="#">Get Started</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


</body>
</html>
