<?php
use yii\widgets\ActiveForm;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Experts</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />
    <!-- FAVICON -->


</head>
<body>


<div class="page-title-area contact-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2><?=Yii::t('common','Контакты')?></h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="#"><?=Yii::t('common','Главная')?></a></li>
                        <li><a href="#"><?=Yii::t('common','Контакты')?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- =========================
  END PAGE TITLE SECTION
============================== -->

<!-- =========================
START CONTACT US SECTION
============================== -->
<section class="contact-us-area contact-us-1-area">
    <div class="contact-form">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="contact-form-3-text">
                        <h2><?=Yii::t('common','Контактная информация')?></h2>
                        <p>Find out about the ways to deal with debts if you are falling behind with day-to-day bills, loan and credit card repayments or other commitments & get some free advice by speaking to one of our financial advisers over the phone? </p>
                        <div class="address-area">
                            <div class="view-location">
                                <h2><?=Yii::t('common','Адрес')?> :</h2>
                                <p><i class="fa fa-map-marker"></i><?=Yii::t('common','г.Москва')?>
                                    <?=Yii::t('common','ул. Орджоникидзе, д.11, стр.2')?></p>
                                <a href="#location"> <?=Yii::t('common','Показать на карте')?></a>
                            </div>
                            <div class="col-md-6 no-padding-left">
                                <div class="address-details">
                                    <h2>Email :</h2>
                                    <span><i class="fa   fa-envelope"></i>GreenCashwm@gmail.com</span>
                                </div>
                            </div>
                            <div class="col-md-6 no-padding-left">
                                <div class="address-details">
                                    <h2><?=Yii::t('common','Телефон')?> :</h2>
                                    <span><i class="fa  fa-phone"></i> +7 (913) 155-09-18</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="contact-form-left contact-form-3 contact-form-right">
                        <h2><?=Yii::t('common','Напишите нам')?></h2>
                        <div class="show_result"></div>
                        <div class="result_message"></div>

                        <? $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($feedback, 'name')->input('text',
                                    ['placeholder'=>Yii::t('common','Имя'), 'id'=>'Name'])->label(false);?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($feedback, 'email')->input('email',
                                    ['placeholder'=>'Email', 'id'=>'Email'])->label(false);?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($feedback, 'telefon')->input('text',
                                    ['placeholder'=>Yii::t('common','Телефон'), 'id'=>'Phone'])->label(false);?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($feedback, 'title')->input('text',
                                    ['placeholder'=>Yii::t('common','Тема'), 'id'=>'Subject'])->label(false);?>
                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($feedback, 'text')->textarea(
                                    ['placeholder'=>Yii::t('common','Сообщение'), 'id'=>'Message'])->label(false);?>
                            </div>
                            <a name="location"></a>
                            <div class="col-sm-12 text-center">
                                <button type="submit"  class="btn btn-dm"><?=Yii::t('common','Отправить сообщение')?></button>
                            </div>
                        </div>

                        <?php  ActiveForm::end(); ?>


                    </div>
                </div>
            </div>
        </div>
        <div class="maps">
            <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae549fe9927bbba7178e10d77039f36a9f3d26349407800fbef58d0ab9e02ab80&amp;width=100%&amp;height=500&amp;lang=ru_RU&amp;scroll=false"></script>
        </div>

    </div>

</section>

</body>
</html>
