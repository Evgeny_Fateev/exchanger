<?php
use yii\widgets\ActiveForm;
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Пополнить</title>
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

</head>
<body>
<div class="page-title-area contact-area">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2><?=Yii::t('common', 'Заявка на пополнение') ?></h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="#"><?=Yii::t('common', 'Главная') ?></a></li>
                        <li><a href="#"><?=Yii::t('common', 'Пополнение') ?></a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
        <section style="background-color: #1bb580;" class="form-area parallax-window" data-parallax="scroll" data-image-src="./images/form-bg.jpg">
            <div class="contact-form">
                <div class="container form-bg">
                    <div class="row">
                        <?php if (Yii::$app->session->hasFlash('success')): ?>
                            <div class="alert alert-success alert-dismissible" role="alert">
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                <?php echo Yii::$app->session->getFlash('success'); ?>
                            </div>
                        <?php endif;?>

                        <?php if (Yii::$app->session->hasFlash('error')): ?>
                        <div class="alert alert-danger alert-dismissible" role="alert">
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <?php echo Yii::$app->session->getFlash('error'); ?>
                            <?php endif;?>
                        </div>
                        <div class="col-sm-12">
                            <div class="contact-form-right">
                                <? $form = ActiveForm::begin();?>
                                    <div class="row">
                                        <div class="col-sm-6">
                                            <?= $form->field($replenish,'name')->input('text',['placeholder' => Yii::t('common', 'Ваше имя:')])->label(false) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($replenish,'email')->input('email',['placeholder' => Yii::t('common', 'Ваш Email:')])->label(false) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <?= $form->field($replenish,'phone')->input('text',['placeholder' => Yii::t('common', 'Номер телефона:')])->label(false) ?>
                                        </div>
                                        <div class="col-sm-6">
                                            <input type="text" readonly class="form-control" placeholder=<?=Yii::t('common', 'Пополнение')?>>
                                        </div>
                                        <div class="col-sm-12">
                                            <?php $c = explode(',', $text_pm->payment_system)  ?>
                                            <select class="option" name="pay_system">
                                                <option value=""><?= Yii::t('common', 'Выберите платежную систему')?></option>

                                                <?php foreach ($c as $m) {

                                                    echo '<option value="'.$m.'">'.Yii::t('common', $m).'</option>';
                                                }

                                                ?>
                                            </select>

                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <button type="submit" class="btn btn-dm"><?=Yii::t('common', 'Отправить')?></button>
                                        </div>
                                        <div class="col-sm-12 text-center">
                                            <input type="checkbox" name="is_confirm" checked> Нажимая кнопку отправить я даю своё согласие на обработку моих персональных данных
                                        </div>
                                    </div>
                                <?php ActiveForm::end();?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
</body>
</html>

