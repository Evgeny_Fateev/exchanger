<?php
use yii\grid\GridView;
use yii\helpers\Html;
 ?>


<div class="navbar navbar-inverse">
    <ul class="nav nav-tabs nav-justified">
        <li><a href="/home/add-posts"><h5>Добавить новость</h5></a> </li>
         <li><a href="/home/add-reviews"><h5>Добавить отзыв</h5></a> </li>
        <li><a href="/transactions/exchang"><h5>Перевести деньги</h5></a> </li>
        <li><a href="/home/add-sliders"><h5>Добавить слайд</h5></a> </li>
        <li><a href="/home/redaction-text"><h5>Редактирование текста/платеж.систем</h5></a> </li>
        <li><a href="/home/change-posts"><h5>Редактирование/удаление новостей, отзывов, слайдов</h5></a> </li>
    </ul>
</div>

<div class="row">
    <div class="col-md-4 col-sm-4">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Подписавшиеся на новости</h3>
            </div>
            <div class="panel-body">
                <?php
                echo GridView::widget([
                    'dataProvider' => $subNews,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'email:ntext',
                        ['class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{delete}{link}',
                            'buttons' => [
                                'delete' => function( $url,$model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/delete-subnews','id'=> $model->id]);
                                    return Html::a('Удалить',$customUrl);
                                },

                            ],
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>

    <div class="col-md-8 col-sm-8">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Обратная связь</h3>
            </div>
            <div class="panel-body">
                <?php
                echo GridView::widget([
                    'dataProvider' => $feedBack,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'id',
                        'name:ntext',
                        'email:ntext',
                        'telefon:ntext',
                        'title:ntext',
                        'text:ntext',
                        ['class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{delete}{link}',
                            'buttons' => [
                                'delete' => function( $url,$model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/delete-feedback','id'=> $model->id]);
                                    return Html::a('Удалить',$customUrl);
                                },

                            ],
                        ],
                    ],
                ]);
                ?>
            </div>
        </div>
    </div>
</div>
<!---->
<div class="panel panel-primary" >
    <div class="panel-heading">
        <h3 class="panel-title">Заявки</h3>
    </div>
    <div class="panel-body">
        <?php
        echo GridView::widget([
            'dataProvider' => $orders,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute'=>'name',
                    'label'=>'Имя',
                ],

                'email:ntext',
                'phone:ntext',
                'type:ntext',
                'payment_system:ntext',
                'is_user:ntext',
                [
                    'attribute'=>'user.email',
                    'label'=>'Зарегистрированный или нет',
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{delete}{link}',
                    'buttons' => [
                        'delete' => function( $url,$model){
                            $customUrl = Yii::$app->getUrlManager()->createUrl(['/home/delete-orders','id'=> $model->id]);
                            return Html::a('Удалить',$customUrl);
                        },

                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>
<div class="panel panel-primary" >
    <div class="panel-heading">
        <h3 class="panel-title">Транзакции</h3>
    </div>
    <div class="panel-body">
        <?php
        echo GridView::widget([
            'dataProvider' => $transactions,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],
                'id',
                [
                    'attribute'=>'wmid',
                    'label'=>'WMID',
                ],
                [
                    'attribute'=>'sender_purse',
                    'label'=>'Кошелек отправки',
                ],
                [
                    'attribute'=>'recipient_purse',
                    'label'=>'Кошелек получателя',
                ],
                [
                    'attribute'=>'number_transaction',
                    'label'=>'Номер транзакции',
                ],
                [
                    'attribute'=>'amount',
                    'label'=>'Сумма',
                ],
                [
                    'attribute'=>'description',
                    'label'=>'Описание',
                ],

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Действия',
                    'template' => '{delete}{link}',
                    'buttons' => [
                        'delete' => function( $url,$model){
                            $customUrl = Yii::$app->getUrlManager()->createUrl(['/home/delete-transactions','id'=> $model->id]);
                            return Html::a('Удалить',$customUrl);
                        },

                    ],
                ],
            ],
        ]);
        ?>

    </div>
</div>