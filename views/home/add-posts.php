<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use dosamigos\tinymce\TinyMce;

?>
<div class="panel panel-primary col-md-12 center-block">
    <div class="panel-heading text-center"><h4>Добавление новости</h4></div>
    <div class="panel-body">
        <?php if (Yii::$app->session->hasFlash('success')): ?>
            <div class="alert alert-success alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash('success'); ?>
            </div>
        <?php endif;?>

        <?php if (Yii::$app->session->hasFlash('error')): ?>
            <div class="alert alert-danger alert-dismissible" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <?php echo Yii::$app->session->getFlash('error'); ?>
            </div>
        <?php endif;?>
        <?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]); ?>
        <?=$form-> field($posts, 'title')->textInput();?>
        <?=$form-> field($posts, 'image')->fileInput();?>
        <?= $form->field($posts, 'text')->widget(TinyMce::className(), [
            'options' => ['rows' => 10],
            'language' => 'ru',
            'clientOptions' => [
                'plugins' => [
                    'advlist autolink lists link charmap hr preview pagebreak',
                    'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                    'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
                ],
                'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
                'external_filemanager_path' => '/web/plugins/responsive_filemanager/filemanager/',
                'filemanager_title' => 'Responsive Filemanager',
                'external_plugins' => [
                    //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                    'filemanager' => '/web/plugins/responsive_filemanager/filemanager/plugin.min.js',
                    //Иконка/кнопка загрузки файла в панеле иснструментов.
                    'responsivefilemanager' => '/web/plugins/responsive_filemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
                ],
                'relative_urls' => false,
            ]
        ]);?>
        <?= $form->field($posts, 'category')->radioList(['Блог'=> 'Блог','Новость'=> 'Новость' ])->label('Категория');?>

        <input type="checkbox" name="send_out" > Разослать пользователям
        <p> <br/><?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?></p>

        <? ActiveForm::end(); ?>
    </div>
</div>


