<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
?>
<?php echo '<div>' .LinkPager::widget(['pagination'=>$pages]). '</div>'; ?>
<button type="button" class="up">Показать старые новости</button>
<button type="button" class="down">Показать новые новости</button>

<div class="row">

<?php foreach ($model as $dat): ?>

    <?php $new_date = Yii::$app->formatter->asDate($dat->date, 'd.n.Y');?>
    <div class="row">
        <a href="<?= yii\helpers\Url::to (['home/sort-news', 'id' => $dat->id])?>">
        <button type="button" class="btn btn-default">
            <?= $new_date ?>
        </button>
        </a>
        <br/>
     </div>


    <?php endforeach;?>
</div>



<?php foreach ($news as $new): ?>
    <div id="news" style="text-align: center;">
        <div class="oneNew">
            <a href="<?= yii\helpers\Url::to (['home/delete', 'id' => $new->id])?>" class="btn btn-danger">Delete</a>
            <a href="<?= yii\helpers\Url::to (['home/update', 'id' => $new->id])?>" class="btn btn-success">Update</a>
            <div class="qw1">
                <?= $new->text ?>
            </div>
                <div class="qw">
                    <?=$new->date ?>
                </div>
        </div>
    </div>
<?php endforeach;?>


<?php
$script = <<< JS
 $('.up').click(function() {
           $(".oneNew").sort(function(a,b){
    return new Date($(a).find(".qw").text()) < new Date($(b).find(".qw").text());
})
    .each(function(){
    $("#news").prepend(this);
})
 });

 
 $('.down').click(function() {
           $(".oneNew").sort(function(a,b){
    return new Date($(a).find(".qw").text()) > new Date($(b).find(".qw").text());
})
    .each(function(){
    $("#news").prepend(this);
})
 });
   
   
   
JS;

$this->registerJs($script);
?>
