<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>

<div class="panel panel-primary col-md-12 center-block"">
    <div class="panel-heading text-center"><h4>Редактирование отзывов</h4></div>
    <div class="panel-body">
		<?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
        	<?= $form->field($reviews, 'name')->textInput();?>
        	<?= $form->field($reviews, 'position')->textInput();?>
        	<?= $form->field($reviews, 'text')->textarea();?>
        	<?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?>
        <?php ActiveForm::end();?>
    </div>
</div>