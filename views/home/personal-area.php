<?php

use app\models\User;
use yii\widgets\ActiveForm;
use yii\grid\GridView;
use yii\helpers\Html;
?>
<?php
if (User::isUserAdmin(Yii::$app->user->identity->email)){
    echo '<a href="/home/admin-panel" class="btn btn-success">Перейти в админку</a>';
}
?>
<div class="row">
    <div class="col-md-6 col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><?=Yii::t('common','Пополнить')?> / <?=Yii::t('common','Вывести')?></h3>
            </div>

            <div class="panel-body">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo Yii::$app->session->getFlash('success'); ?>
                    </div>
                <?php endif;?>

                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo Yii::$app->session->getFlash('error'); ?>
                    </div>
                <?php endif;?>
                <? $form = ActiveForm::begin();?>
                <div class="col-sm-6">
                    <?= $form->field($order,'name')->input('text',['placeholder' => Yii::t('common', 'Ваше имя:')])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($order,'email')->input('email',['placeholder' => Yii::t('common', 'Ваш Email:')])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($order,'phone')->input('text',['placeholder' => Yii::t('common', 'Номер телефона:')])->label(false) ?>
                </div>
                <div class="col-sm-6">
                    <?= $form->field($order, 'type')
                        ->dropDownList([
                            'Пополнение' => Yii::t('common', 'Пополнить'),
                            'Вывод' =>Yii::t('common', 'Вывести'),
                        ],
                            [
                                'prompt' => Yii::t('common', 'Что хотите сделать?')
                            ])->label(false);?>
                </div>
                <div class="col-sm-12">
                    <?= $form->field($order, 'payment_system')->textarea(['placeholder' => Yii::t('common', 'Ваш вариант платежной системы'), 'rows'=>3])->label(false);?>

                </div>
                <div class="col-sm-12 text-center">
                    <button type="submit" class="btn btn-dm"><?=Yii::t('common', 'Отправить')?></button>
                </div>
                <div class="col-sm-12 text-center">
                    <input type="checkbox" name="is_confirm" checked> Нажимая кнопку отправить я даю своё согласие на обработку моих персональных данных
                </div>
                <? ActiveForm::end();?>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-sm-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center"><?=Yii::t('common','Перевести средства')?></h3>
            </div>

            <div class="panel-body">
                <?php
                //кошелек на который будут перечислены средства
                $wm_purce = "R200348960891";
                //Массив данных по текущему заказу.

                if( isset($_POST['LMI_PREREQUEST']) && $_POST['LMI_PREREQUEST'] == 1) {


                    if ( $rows != 1 ) {
                        exit('Order not faund');
                    }
                    else {


                        if($_POST['LMI_PAYMENT_NO'] == $row['id']
                            && $_POST['LMI_PAYEE_PURSE'] == $wm_purce
                            && $_POST['LMI_PAYMENT_AMOUNT'] == $row['price']) {

                            echo 'YES';

                        }
                        else {
                            exit();
                        }
                    }
                }

                ?>

                <p>Для подтверждения нажмите кнопку оплатить.</p>
                <form method="POST" action="https://merchant.webmoney.ru/lmi/payment.asp" accept-charset="windows-1251" >
                    <input type="input" name="LMI_PAYMENT_AMOUNT"  placeholder="Введите сумму(дробная часть через точку)">
                    <input type="email" name="LMI_PAYMENT_DESC" placeholder="Введите ваш email">
                    <input type="hidden" name="LMI_PAYMENT_NO" value="1234">
                    <input type="hidden" name="LMI_PAYEE_PURSE" value="R200348960891">
                    <input type="hidden" name="LMI_SIM_MODE" value="0">

                    <input type="submit"  value="Оплатить">
                </form>
            </div>
    </div>
    </div>
</div>


<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title text-center"><?=Yii::t('common', 'История заказов')?></h3>
             </div>
            <div class="panel-body">
            <?php echo GridView::widget([
                    'dataProvider' => $history_orders,
                    'columns' => [
                    'name:ntext',
                    'email:ntext',
                    'phone:ntext',
                    'type:ntext',
                    'payment_system:ntext',
                ],
            ]);?>
            <div class="col-sm-3 col-md-3">

            <?php if ($is_subNews == 1 ):?>
                <h4><?=Yii::t('common', 'Вы подписаны на новости')?></h4>
             <?php else: ?>
                <h4><?=Yii::t('common', 'Вы не подписаны на новости')?></h4>
                <? $form = ActiveForm::begin();?>
                <?=$form->field($sub_news_save,'email')->input('email',['placeholder' =>Yii::t('common', 'Впишите свой email') ])->label(false);?>
                <?=Html::submitButton(Yii::t('common', 'Подписаться на новости'), ['class'=>'btn btn-success']);?>
                <? ActiveForm::end();?>
            <?php endif;?>

            </div>
            </div>
        </div>
    </div>
</div>

