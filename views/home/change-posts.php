<?php 
use yii\grid\GridView;
use yii\helpers\Html;
use yii\helpers\Url;

?>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Статьи</h3>
             </div>
            <div class="panel-body">
            <?php echo GridView::widget([
                    'dataProvider' => $posts,
                    'columns' => [
                    'title:ntext',
                    'text:raw',
                    'date:ntext',
                    'category:ntext',
                     ['class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{delete}{link}, {update}{link}',
                            'buttons' => [
                                'delete' => function( $url,$model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/delete-posts','id'=> $model->id]);
                                    return Html::a('Удалить',$customUrl);
                                },
                                'update' => function($url, $model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/update-posts', 'id' => $model->id]);
                                    return Html::a('Редактировать',$customUrl);
                                }

                            ],
                        ],
                ],
            ]);?>
             
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                 <h3 class="panel-title text-center">Отзывы</h3>
             </div>
            <div class="panel-body">
            <?php echo GridView::widget([
                    'dataProvider' => $reviews,
                    'columns' => [
                    'name:ntext',
                    'position:ntext',
                    'text:ntext',
                    ['class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{delete}{link}, {update}{link}',
                            'buttons' => [
                                'delete' => function( $url,$model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/delete-reviews','id'=> $model->id]);
                                    return Html::a('Удалить',$customUrl);
                                },
                                'update' => function($url, $model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/update-reviews', 'id' => $model->id]);
                                    return Html::a('Редактировать',$customUrl);
                                }

                            ],
                        ],
            
                ],
            ]);?>
            
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-md-12 col-sm-12">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title text-center">Отзывы</h3>
            </div>
            <div class="panel-body">
                <?php echo GridView::widget([
                    'dataProvider' => $sliders,
                    'columns' => [
                        [
                            'label' => 'Картинка',
                            'format' => 'raw',
                            'value' => function($data){
                                return Html::img(Url::toRoute($data->image),[
//
                                    'style' => 'width:250px;'
                                ]);
                            },
                        ],
                        'text:raw',
                        ['class' => 'yii\grid\ActionColumn',
                            'header' => 'Действия',
                            'template' => '{delete}{link}, {update}{link}',
                            'buttons' => [
                                'delete' => function( $url,$model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/delete-slider','id'=> $model->id]);
                                    return Html::a('Удалить',$customUrl);
                                },
                                'update' => function($url, $model){
                                    $customUrl = Yii::$app->getUrlManager()->createUrl(['home/update-slider', 'id' => $model->id]);
                                    return Html::a('Редактировать',$customUrl);
                                }
                            ],
                        ],

                    ],
                ]);?>

            </div>
        </div>
    </div>
</div>