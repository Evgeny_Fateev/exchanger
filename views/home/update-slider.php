<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\LinkPager;
use dosamigos\tinymce\TinyMce;
?>

<div class="panel panel-primary col-md-12 center-block"">
<div class="panel-heading text-center"><h4>Редактирование новости</h4></div>
<div class="panel-body">
    <?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]);?>
    <?= $form->field($sliders, 'image')->fileInput();?>
    <div style="width: 300px; height: 220px;">
        <? echo Html::img('@web/'.$sliders->image, ['style' =>'max-width:100%;max-height: 100%; border: 1px solid black'])?>
    </div>
    <?= $form->field($sliders, 'text')->widget(TinyMce::className(), [
        'options' => ['rows' => 10],
        'language' => 'ru',
        'clientOptions' => [
            'plugins' => [
                'advlist autolink lists link charmap hr preview pagebreak',
                'searchreplace wordcount textcolor visualblocks visualchars code fullscreen nonbreaking',
                'save insertdatetime media table contextmenu template paste image responsivefilemanager filemanager',
            ],
            'toolbar' => 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | responsivefilemanager link image media',
            'external_filemanager_path' => '/web/plugins/responsive_filemanager/filemanager/',
            'filemanager_title' => 'Responsive Filemanager',
            'external_plugins' => [
                //Иконка/кнопка загрузки файла в диалоге вставки изображения.
                'filemanager' => '/web/plugins/responsive_filemanager/filemanager/plugin.min.js',
                //Иконка/кнопка загрузки файла в панеле иснструментов.
                'responsivefilemanager' => '/web/plugins/responsive_filemanager/tinymce/plugins/responsivefilemanager/plugin.min.js',
            ],
            'relative_urls' => false,
        ]
    ]);?>

    <?= Html::submitButton('Добавить', ['class'=>'btn btn-success']) ?>
    <?php  ActiveForm::end(); ?>
</div>

