<?php
use yii\helpers\Html;
use yii\helpers\StringHelper;
use yii\widgets\ActiveForm;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->


    <!-- FAVICON -->

    <!-- GOOGLE FONT -->
<!--    <link href='https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900' rel='stylesheet' type='text/css'>-->
<!--    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' rel='stylesheet' type='text/css'>-->
<!--    <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic' rel='stylesheet' type='text/css'>-->

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>-->
    <![endif]-->
</head>
<body>




<!-- =========================
  END HEADER SECTION
============================== -->

<!-- START REVOLUTION SLIDER 5.0 -->
<div class="rev_slider_wrapper rev_slider_wrapper-1">
    <div id="slider1" class="rev_slider"  data-version="5.0">
        <ul>	<!-- SLIDE  -->
            <li data-index="rs-1" data-transition="fade" data-slotamount="default" data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"  data-title="Slide" data-param1="" data-param2="" data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/web/slider/slide1.jpg"  alt="" title="bg-slider1"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYERS -->
                <div class="tp-caption   tp-resizeme  ft-droid-b-i"
                     id="slide-1-layer-1"
                     data-x="center" data-hoffset=""
                     data-y="320"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:500;e:easeOutCirc;"
                     data-transform_out="y:-50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                     data-start="700"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 5; white-space: nowrap; font-size: 17px; line-height: 17px; font-weight: 400; color: rgba(229, 228, 228, 1.00);">Аттестованный обменный пункт системы WebMoney Transfer! </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-1-layer-2"
                     data-x="center" data-hoffset=""
                     data-y="368"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="x:50px;opacity:0;s:500;e:easeOutCubic;"
                     data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="900"
                     data-splitin="chars"
                     data-splitout="none"
                     data-responsive_offset="on"
                     data-elementdelay="0.1"
                     style="z-index: 6; white-space: nowrap; font-size: 55px; line-height: 50px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">Акция! Вывод WMZ - 1%! WMR - 2%! </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-1-layer-3"
                     data-x="center" data-hoffset=""
                     data-y="437"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="3000"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 7; white-space: nowrap; font-size: 17px; line-height: 26px; font-weight: 400; color: rgba(229, 228, 228, 1.00);font-family:Raleway;"><div class="text-center">В электронном обменнике «GreenCash» вы можете пополнить кошелек WebMoney, <br />а также обменять любую валюту вебмани за наличные или на банковскую карту по самым выгодным тарифам.</div> </div>

                <!-- LAYER NR. 4 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-1-layer-4"
                     data-x="center" data-hoffset="-137"
                     data-y="526"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="x:-50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                     data-start="3300"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 8; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);"><a href="/page/replenish" class="btn rev-slider-btn">Пополнить</a> </div>

                <!-- LAYER NR. 5 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-1-layer-5"
                     data-x="center" data-hoffset="98"
                     data-y="526"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="x:50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="x:50px;opacity:0;s:300;s:300;"
                     data-start="3300"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 9; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);"><a href="/page/output" class="btn rev-slider-btn rev-slider-btn-2">Вывести</a>
                </div>
            </li>
            <!-- SLIDE  -->
            <li data-index="rs-24" data-transition="fade" data-slotamount="default" data-hideafterloop="0"
                data-hideslideonmobile="off"  data-easein="default" data-easeout="default" data-masterspeed="300"
                data-rotate="0"  data-saveperformance="off"  data-title="Home2" data-param1="" data-param2=""
                data-param3="" data-param4="" data-param5="" data-param6="" data-param7="" data-param8="" data-param9=""
                data-param10="" data-description="">
                <!-- MAIN IMAGE -->
                <img src="/web/images/rev-slider2.jpg"  alt="" title="bg-slider2"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                <!-- LAYER NR. 1 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-24-layer-2"
                     data-x="center" data-hoffset=""
                     data-y="302"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:-50px;opacity:0;s:500;e:easeOutCubic;"
                     data-transform_out="y:-50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="700"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 5; white-space: nowrap; font-size: 55px; line-height: 60px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Raleway;"><div class="text-center">Financial Security<br/>& Peace of Mind for Your Family</div> </div>

                <!-- LAYER NR. 2 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-24-layer-3"
                     data-x="center" data-hoffset=""
                     data-y="448"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="y:50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="1000"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 6; white-space: nowrap; font-size: 17px; line-height: 26px; font-weight: 400; color: rgba(229, 228, 228, 1.00);font-family:Raleway;"><div class="text-center">The choice is in your hands as to where you go to get advice and where you buy your<br/> financial products. We are not pushy, so we don’t want to convince you to useus !</div> </div>

                <!-- LAYER NR. 3 -->
                <div class="tp-caption   tp-resizeme"
                     id="slide-24-layer-4"
                     data-x="center" data-hoffset=""
                     data-y="532"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="x:-50px;opacity:0;s:500;e:easeOutCirc;"
                     data-transform_out="x:-50px;opacity:0;s:300;s:300;"
                     data-start="1300"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 7; white-space: nowrap; font-size: 20px; line-height: 22px; font-weight: 400; color: rgba(255, 255, 255, 1.00);"><a href="#" class="btn rev-slider-btn">Explore Our Services</a> </div>
            </li>
            <!-- SLIDE  -->
            <?php
            $slides_iterator = 25;
            $layers_iterator = 2;
            ?>
            <?php foreach ($sliders as $slider): ?>
            <li data-index="rs-<?=$slides_iterator?>" data-transition="fade" data-slotamount="default"
                data-hideafterloop="0" data-hideslideonmobile="off"  data-easein="default"
                data-easeout="default" data-masterspeed="300"  data-rotate="0"  data-saveperformance="off"
                data-title="Home6" data-param1="" data-param2="" data-param3="" data-param4="" data-param5=""
                data-param6="" data-param7="" data-param8="" data-param9="" data-param10="" data-description="">

                <!-- MAIN IMAGE -->


<!--                <img src="/web/images/rev-slider3.jpg"  alt="" title="bg-slider6"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>-->
                <!-- LAYERS -->
<!--                <div class="tp-caption   tp-resizeme"-->
<!--                     id="slide-25-layer-8"-->
<!--                     data-x="center" data-hoffset="-100"-->
<!--                     data-y="338"-->
<!--                     data-width="['none','none','none','none']"-->
<!--                     data-height="['none','none','none','none']"-->
<!--                     data-transform_idle="o:1;"-->
<!--                     data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"-->
<!--                     data-transform_out="x:-50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"-->
<!--                     data-start="700"-->
<!--                     data-responsive_offset="on"-->
<!--                     style="z-index: 5;"><img src="/web/images/rev-icon-1.png" alt="" width="80" height="80" data-ww="70px" data-hh="70px" data-no-retina> </div>-->
<!---->
<!--                <!-- LAYER NR. 2 -->-->
<!--                <div class="tp-caption   tp-resizeme"-->
<!--                     id="slide-25-layer-7"-->
<!--                     data-x="center" data-hoffset="1"-->
<!--                     data-y="338"-->
<!--                     data-width="['none','none','none','none']"-->
<!--                     data-height="['none','none','none','none']"-->
<!--                     data-transform_idle="o:1;"-->
<!--                     data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"-->
<!--                     data-transform_out="y:-50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"-->
<!--                     data-start="1200"-->
<!--                     data-responsive_offset="on"-->
<!--                     style="z-index: 6;"><img src="/web/images/rev-icon-2.png" alt="" width="80" height="80" data-ww="70px" data-hh="70px" data-no-retina> </div>-->
<!---->
<!--                <!-- LAYER NR. 3 -->-->
<!--                <div class="tp-caption   tp-resizeme"-->
<!--                     id="slide-25-layer-9"-->
<!--                     data-x="center" data-hoffset="100"-->
<!--                     data-y="338"-->
<!--                     data-width="['none','none','none','none']"-->
<!--                     data-height="['none','none','none','none']"-->
<!--                     data-transform_idle="o:1;"-->
<!--                     data-transform_in="z:0;rX:0;rY:0;rZ:0;sX:0.8;sY:0.8;skX:0;skY:0;opacity:0;s:1500;e:Power4.easeOut;"-->
<!--                     data-transform_out="x:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"-->
<!--                     data-start="1600"-->
<!--                     data-responsive_offset="on"-->
<!--                     style="z-index: 7;"><img src="/web/images/rev-icon-3.png" alt="" width="80" height="80" data-ww="70px" data-hh="70px" data-no-retina> </div>-->

                <!-- LAYER NR. 4 -->
                <? echo Html::img('@web/'.$slider->image,['style'=> 'title="bg-slider6"  width="1600" height="800" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina']);?>

                <div class="tp-caption   tp-resizeme"
                     id="slide-<?=$slides_iterator?>-layer-<?=$layers_iterator?>"
                     data-x="center" data-hoffset=""
                     data-y="524"
                     data-width="['auto']"
                     data-height="['auto']"
                     data-transform_idle="o:1;"
                     data-transform_in="x:50px;opacity:0;s:500;e:easeOutCubic;"
                     data-transform_out="y:50px;opacity:0;s:300;e:easeOutCirc;s:300;e:easeOutCirc;"
                     data-start="1000"
                     data-splitin="none"
                     data-splitout="none"
                     data-responsive_offset="on"
                     style="z-index: 8; white-space: nowrap; font-size: 55px; line-height: 60px; font-weight: 700; color: rgba(255, 255, 255, 1.00);font-family:Raleway;">
                    <div class="text-center">
                        <?=$slider->text?>
                    </div>
                </div>
                <?php
                    $slides_iterator++;
                    $layers_iterator++;
                ?>

            </li>
            <?php endforeach;?>
        </ul>
    </div>
</div><!-- END OF SLIDER WRAPPER -->

<!-- =========================
  START WELCOME SECTION
============================== -->
<section class="welcome-area">
    <!-- MAIN TITLE -->
    <div class="main-title welcome-main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">

                        <h2>«GREENCASH» - ЛУЧШИЙ ОБМЕННИК электронных ВАЛЮТ!</h2>
                        <p>
                            <?=strip_tags($text->text)?>
                        </p>
<!--                        <p>Бизнес в интернете развивается бешеными темпами. В сети предлагается огромное количество товаров и услуг – от косметики до обучения иностранных языков. Работая в электронной сети, без электронных денег - не обойтись. Деньги хранятся на счетах той или иной электронной системы, и периодически возникает необходимость обменивать электронные деньги на те, которые легко обналичивать или же наоборот – тратить в Сети.<br>-->
<!--                            Соответственно возрастает и потребность в онлайн-обменнике валют Электронную валюту, как и любую другую кроме того, чтобы тратить, нужно так же и выгодно обменять. В связи с этим возникает большое количество онлайн-сервисов для обмена электронных денег. Это сервисы – очень удобные.</p>-->

                        <br /><h2>Преимущества онлайн - обменников:</h2>

                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->

    <!-- WELCOME CONTENT -->
    <div class="welcome-content-area">
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="welcome-content">
                        <h2>Работают круглые сутки</h2>

                        <span>01.</span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="welcome-content">

                        <h2>Наблюдают за обменным курсом очень большого количества обменных пунктов в интернете</h2>

                        <span>02.</span>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="welcome-content">

                        <h2>Предоставляют актуальную информацию быстро и совершенно бесплатно</h2>

                        <span>03.</span>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="common-btn welcome-title-cmn-btn">
                        <div class="col-sm-6">
                            <div class="left-cmn-btn">
                                <a href="http://wiki.webmoney.ru/">О нашем сервисе</a>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="right-cmn-btn">
                                <a href="#application"><?=Yii::t('common', 'Оставить заявку')?></a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END WELCOME SECTION
============================== -->

<!-- =========================
  START EXPERTISE SECTION
============================== -->
<section class="expertise-area bg-type-2">
    <div class="container expertise-area-details">
        <div class="row">
            <div class="col-md-6">
                <div class="expert-left-layer match_item">
                    <div class="menuzord-brand-type-2">
                        <img src="/web/images/logo.png" alt="">
                    </div>
                    <!--<h2>Ввод/вывод Webmoney для юридических и физических лиц</h2>-->
                    <p><?=Yii::t('common', 'В электронном обменнике «GreenCash» вы можете пополнить кошелек WebMoney, а также обменять любую валюту вебмани за наличные или на банковскую карту по самым выгодным тарифам.')?>
                        <br /><?=Yii::t('common', 'Для совершения денежного перевода и обмена вебмани необходимо заполнить бланк электронной заявки. Это займет не больше минуты. В ближайшее время менеджер «GreenCash» свяжется с вами для уточнения деталей сделки.')?>
                        <br /><?=Yii::t('common', 'При совершении денежных операций всегда присутствует определенный риск. Гарантом нашей порядочности и безопасности сделки служит аттестация, произведенная со стороны WebMoney Transfer, с подтверждением реквизитов и удостоверением личности.')?></p>
                    <h2><?=Yii::t('common', 'Ввод/вывод Webmoney для юридических и физических лиц')?></h2>
                    <p><?=Yii::t('common', 'Наш онлайн обменник валют предоставляет услуги обмена WebMoney физическим и юридическим лицам. Для уточнения деталей работы с Вебмани для юридических лиц свяжитесь любым удобным способом с консультантом «GreenCash»')?>
                    </p>
                    <!--<div class="common-btn">
                        <div class="left-cmn-btn btn-expert-left">
                            <a href="#">Юридическим лицам</a>
                        </div>
                    </div>-->
                </div>
            </div>
            <div class="col-md-6">
                <h2><?=Yii::t('common', 'Правила системы')?>.</h2>
                <div class="expert-right match_item">
                    <div class="expert-right-content clearfix">
                        <div class="expert-right-first-content clearfix">
                            <div class="expert-right-single-content e-r-s-padding-btm e-r-s-right-padding">

                                <p><?=Yii::t('common', 'Минимальная сумма для ввода/вывода вебмани – 100 wmr, 5 wmz или 5 wme')?>.</p>
                            </div>
                            <div class="expert-right-single-content e-r-s-padding-btm e-r-s-border e-r-s-left-padding">


                                <p><?=Yii::t('common', 'Среднее время обработки заявки – 2 минуты')?>.</p>
                            </div>
                        </div>
                        <div class="expert-right-single-content e-r-s-padding e-r-s-right-padding">


                            <p><?=Yii::t('common', 'Если денежные средства после автоматического обмена Вебмани в течение часа не поступили на счет, обратитесь за помощью в службу поддержки')?>.</p>
                        </div>
                        <div class="expert-right-single-content e-r-s-padding e-r-s-left-padding e-r-s-border">


                            <p><?=Yii::t('common', 'Сотрудничаем только с резидентами РФ (кроме Золотой Короны, ВТБ24, Western Union)')?>.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END EXPERTISE SECTION
============================== -->

<!-- =========================
  START SERVICE SECTION
============================== -->

<!-- =========================
  END SERVICE SECTION
============================== -->

<!-- =========================
  START BLOG SECTION
============================== -->
<section class="blog-area bg-type-2">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h3><?=Yii::t('common', 'Новости, советы и многое другое')?>!</h3>
                        <h2><?=Yii::t('common', 'Наш блог')?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="blog-content-area">
        <div class="container">
            <div class="row">
                <?php foreach ($posts as $post):?>
                <div class="col-md-4 col-sm-4">
                    <div class="blog-content-single">
                        <div class="blog-img">
                            <div  style="width: 298px;height: 240px;">
                                <? echo Html::img('@web/'.$post->image, ['style' =>'max-width:100%;max-height: 100%;border: 1px solid black'])?>

                            </div>
<!--                            <img src="/web/images/blog/blog-1.png" alt="" class="img-responsive">-->
                            <i class="fa fa-image"></i>
                        </div>
                        <div class="blog-text">
                            <ul>
                                <li><?=$post->date?></li>
                                <li>
                                    <a href="#"><?=$post->category?></a>
                                </li>
                            </ul>
                            <h2><a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $post->id])?>"><?=$post->title?></a></h2>
                            <p><?=StringHelper::truncate(preg_replace("/<img[^<]+/u", '',$post->text), 100, '...')?></p>
                            <a href="<?= yii\helpers\Url::to (['page/single-post', 'id' => $post->id])?>"><i class="fa fa-long-arrow-right"></i> <span><?=Yii::t('common', 'Подробнее')?></span></a>
                        </div>
                    </div>
                </div>
                <?php endforeach;?>

            </div>
            <div class="row">
                <div class="blog-explore text-center">
                    <a href="/page/all-posts"><i class="fa fa-long-arrow-right"></i> <?=Yii::t('common', 'Читать все записи')?> </a>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END BLOG SECTION
============================== -->

<!-- =========================
  START FORM SECTION
============================== -->

<section style="background-color: #1bb580;" class="form-area parallax-window" data-parallax="scroll" data-image-src="./images/form-bg.jpg">
    <a name ="application"></a>
    <div class="contact-form">
        <div class="container form-bg">
            <div class="row">
                <?php if (Yii::$app->session->hasFlash('success')): ?>
                    <div class="alert alert-success alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo Yii::$app->session->getFlash('success'); ?>
                    </div>
                <?php endif;?>

                <?php if (Yii::$app->session->hasFlash('error')): ?>
                    <div class="alert alert-danger alert-dismissible" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <?php echo Yii::$app->session->getFlash('error'); ?>
                    </div>
                <?php endif;?>

                <div class="col-sm-5">
                    <div class="form-left-content">
                        <div class="main-title main-title-left">
                            <div class="main-title-content">
                                <h3><?=Yii::t('common', 'Остались вопросы')?>?</h3>
                                <h2><?=Yii::t('common', 'Оставьте заявку')?></h2>
                                <p>Использование электронных обменников делает жизнь проще и динамичнее. Выгодный обмен электронной валюты – реален. И реален он с нами!</p>
                            </div>
                        </div>
                        <div class="form-left-address">
                            <? echo Html::img('/web/images/frayNewSmall.png', ['style' =>'max-width:150px;max-height: 150px'])?>

                            <!--                            --><?//=Yii::t('common', 'Мы работаем')?><!--: <br>-->
<!--                            --><?//=Yii::t('common', '24 часа')?><!-- <br>-->
<!--                            --><?//=Yii::t('common', '7 дней в неделю')?><!--<br />-->
<!--                            --><?//=Yii::t('common', '365 дней в году')?>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="contact-form-right">
                        <? $form = ActiveForm::begin();?>
                        <div class="row">
                            <div class="col-sm-6">
                                <?= $form->field($order,'name')->input('text',['placeholder' => Yii::t('common', 'Ваше имя:')])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($order,'email')->input('email',['placeholder' =>Yii::t('common', 'Ваш Email:')])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($order,'phone')->input('text',['placeholder' =>Yii::t('common', 'Номер телефона:')])->label(false) ?>
                            </div>
                            <div class="col-sm-6">
                                <?= $form->field($order, 'type')
                                    ->dropDownList([
                                        'Пополнение' => Yii::t('common', 'Пополнить'),
                                        'Вывод' =>Yii::t('common', 'Вывести'),
                                    ],
                                        [
                                            'prompt' => Yii::t('common', 'Что хотите сделать?')
                                        ])->label(false);?>
                            </div>
                            <div class="col-sm-12">
                                <?= $form->field($order, 'payment_system')->textarea(['placeholder' =>Yii::t('common', 'Ваш вариант платежной системы') , 'rows'=>3])->label(false);?>

                            </div>
                            <div class="col-sm-12 text-center">
                                <button type="submit" class="btn btn-dm"><?=Yii::t('common', 'Отправить')?></button>
                            </div>
                            <div class="col-sm-12 text-center">
                                <input type="checkbox" name="is_confirm" checked> Нажимая кнопку отправить я даю своё согласие на обработку моих персональных данных
                            </div>
                        </div>
                        <?php ActiveForm::end();?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END FORM SECTION
============================== -->

<!-- =========================
  START QUOTE 2 SECTION
============================== -->
<section class="quote-area  quote-3-area bg-type-2">
    <div class="container-fluid">
        <div class="row">
            <div class="col-md-12 no-padding">
                <div id="quote-3-slider" class="owl-carousel all-carousel owl-theme">
                    <!--<div class="quote-single quote-3-single">
                        <div class="quote-main-content quote-3-main-content">
                            <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                            <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                            <p>Воспользовался услугами данного обменника и остался очень доволен</p>
                        </div>
                        <div class="quote-author quote-3-author">
                            <div class="quote-author-details">
                                <h3>Иван И.</h3>
                                <p>Предприниматель</p>
                            </div>
                        </div>
                    </div>
                    <div class="quote-single quote-3-single">
                        <div class="quote-main-content quote-3-main-content">
                            <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                            <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                            <p>Ребята работают по очень выгодному курсу! Доволен скоростью обработки моей заявки</p>
                        </div>
                        <div class="quote-author quote-3-author">
                            <div class="quote-author-details">
                                <h3>Сергей Ч.</h3>
                                <p>Фрилансер</p>
                            </div>
                        </div>
                    </div>
                    <div class="quote-single quote-3-single">
                        <div class="quote-main-content quote-3-main-content">
                            <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                            <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                            <p>Пользуюсь услугами регулярно. Greencash действительно делают мою жизнь проще.</p>
                        </div>
                        <div class="quote-author quote-3-author">
                            <div class="quote-author-details">
                                <h3>Артур</h3>
                                <p>Предприниматель</p>
                            </div>
                        </div>
                    </div>-->
                    <?php foreach ($reviews as $review): ?>
                    <div class="quote-single quote-3-single">
                        <div class="quote-main-content quote-3-main-content">
                            <span class="small-quote"><i class="fa fa-quote-left"></i></span>
                            <span class="large-quote"><i class="fa fa-quote-left"></i></span>
                            <p><?=$review->text?></p>
                        </div>
                        <div class="quote-author quote-3-author">
                            <div class="quote-author-details">
                                <h3><?=$review->name?></h3>
                                <p><?=$review->position?></p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>

                </div>
            </div>

            <div class="common-btn">
                <div class="left-cmn-btn btn-expert-left">
                    <center> <a style="background-color: #4c9122;" href="#application"><?=Yii::t('common', 'Оставить заявку')?></a>   </center>
                </div>
            </div>


        </div>
    </div>
</section>

<!-- =========================
  END QUOTE 2 SECTION
============================== -->

<!-- =========================
  START QUOTE SECTION
============================== -->
<section class="client-area">
    <!-- MAIN TITLE -->
    <div class="main-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h2><?=Yii::t('common', 'Наши партнеры')?></h2>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div id="client-slider" class="owl-carousel all-carousel owl-theme">
                    <div class="client-single">
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/alfa.jpg" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/sber.jpg" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/bitcoin.jpg" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/tinkoff.png" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/perfect_money.png" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/webmoney.png" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                    </div>
                    <div class="client-single">
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/qiwi.png" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                        <div class="client-img" style="width: 150px;height: 232px;>
                            <a href="#"><span><img src="/web/images/logo/yandex.png" style="max-height: 100%;max-width: 100%;" alt=""></span></a>
                        </div>
                    </div>



<!--                    <div class="client-single">-->
<!--                        <div class="client-img">-->
<!--                            <a href="#"><span><img src="/web/images/logo/perfect_money.png" alt=""></span></a>-->
<!--                        </div>-->
<!--                        <div class="client-img">-->
<!--                            <a href="#"><span><img src="/web/images/logo/qiwi.png" alt=""></span></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="client-single">-->
<!--                        <div class="client-img">-->
<!--                            <a href="#"><span><img src="/web/images/client/client-3.png" alt=""></span></a>-->
<!--                        </div>-->
<!--                        <div class="client-img">-->
<!--                            <a href="#"><span><img src="/web/images/client/client-7.png" alt=""></span></a>-->
<!--                        </div>-->
<!--                    </div>-->
<!--                    <div class="client-single">-->
<!--                        <div class="client-img">-->
<!--                            <a href="#"><span><img src="/web/images/client/client-4.png" alt=""></span></a>-->
<!--                        </div>-->
<!--                        <div class="client-img">-->
<!--                            <a href="#"><span><img src="/web/images/client/client-8.png" alt=""></span></a>-->
<!--                        </div>-->
<!--                    </div>-->

                </div>
            </div>
        </div>
    </div>
</section>
<!-- =========================
  END QUOTE SECTION
============================== -->


</body>
</html>