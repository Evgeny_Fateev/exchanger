<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- TITLE -->
    <title>Ошибка</title>
    <!-- FAVICON -->
    <link rel="shortcut icon" type="image/x-icon" href="favicon.ico" />

</head>
<body>

<div class="page-title-area feature-call-action-bg">
    <div class="container">
        <div class="row">
            <div class="col-sm-6 col-md-6">
                <div class="page-title-left">
                    <h2>Error Page</h2>
                </div>
            </div>
            <div class="col-sm-6 col-md-6">
                <div class="page-bredcrumbs-area text-right">
                    <ul  class="page-bredcrumbs">
                        <li><a href="/">Home</a></li>
                        <li><a href="#">Features</a></li>
                        <li><a href="#">404</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<
<section class="feature-404-page">
    <!-- MAIN TITLE -->
    <div class="main-title feature-404-title">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="main-title-content text-center">
                        <h2>404</h2>
                        <p>With any financial product that you buy, it is important that you know you are getting the best advice from a reputable company as often you will have to provide sensitive information online or over the internet.</p>
                        <a href="/">Back To Homepage</a>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- END MAIN TITLE -->
</section>

</body>
</html>