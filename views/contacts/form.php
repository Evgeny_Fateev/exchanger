<?php
use yii\widgets\ActiveForm;
use yii\helpers\Html;
?>
<div class="contacts">
    <div class="row">
        <h4>Адрес: <?= $contacts->address ?></h4>
    </div>
    <div class="row">
        <h4>Телефон: <?= $contacts->telefon ?></h4>
    </div>
    <div class="row">
        <h4>E-mail: <?= $contacts->email ?></h4>
    </div>
    <div class="row">
        <h4>Skype: <?= $contacts->skype ?></h4>
    </div>
    <div class="row">
        <h4>Telegram: <?= $contacts->telegram ?></h4>
    </div>
</div>
<div class="maps">
    <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?um=constructor%3Ae549fe9927bbba7178e10d77039f36a9f3d26349407800fbef58d0ab9e02ab80&amp;width=1200&amp;height=600&amp;lang=ru_RU&amp;scroll=true"></script>
</div>
<div class="panel panel-primary col-md-3 center-block">
    <div class="panel-heading text-center"><h4>Обратная связь</h4></div>
    <div class="panel-body">
        <?$form = ActiveForm::begin();?>
        <?=$form->field($form_fb,'name')->textInput() ?>
        <?=$form->field($form_fb,'email')->textInput() ?>
        <?=$form->field($form_fb,'text')->textarea()?>
        <?= Html::submitButton('Отправить', ['class'=>'btn btn-success'])?>
        <? ActiveForm::end();?>
    </div>
</div>
