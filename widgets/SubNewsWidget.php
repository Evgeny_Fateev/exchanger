<?php
namespace  app\widgets;

use app\models\SubscriptionNews;
use Yii;
use yii\base\Widget;
class SubNewsWidget extends Widget
{

    public function run() {
        $sub_news = new SubscriptionNews();
        if($sub_news->load(Yii::$app->request->post())){
            if($sub_news->save()){
            $sub_news->email = \Yii::$app->mailer->compose()
                ->setTo('zhenja1509@gmail.com')
                ->setFrom('zhenja94@bk.ru')
                ->setSubject('Подписка на новости')
                ->setTextBody("На ваши новости подписался " .$sub_news->email)
                ->send();
            }

        }

        return $this->render('subnews',compact('sub_news'));

    }



}