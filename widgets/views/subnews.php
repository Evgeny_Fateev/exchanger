<?php
use yii\widgets\ActiveForm;
?>
<div class="form-group footer-subscribe">
    <?$form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data', 'id'=>'newsletter-signup']]);?>
    <input type="email" name="SubscriptionNews[email]" class="form-control" placeholder=<?=Yii::t('common','Подписаться на новости')?>>
    <button type="submit" class="btn btn-default"><i class="fa fa-long-arrow-right"></i></button>
    <?php  ActiveForm::end(); ?>
</div>