<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace app\assets;

use yii\web\AssetBundle;

/**
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        "css/bootstrap.min.css",
        "css/font-awesome.css",
        "css/menuzord.css" ,
        "css/stroke-icon.css",
        "css/demo.css" ,
        "css/ie7.css" ,
        "css/bootFolio.css",
        "css/magnific-popup.css",
        "css/jquery-ui.css",
        "css/owl.theme.default.css",
        "css/owl.carousel.css",
        "web/revolution/css/settings.css",
        "web/revolution/css/layers.css",
        "web/revolution/css/navigation.css",
        "css/style.css",
       'https://fonts.googleapis.com/css?family=Raleway:400,100,200,300,500,600,700,800,900',
    'https://fonts.googleapis.com/css?family=Droid+Serif:400,400italic,700,700italic' ,
    'https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,600italic,700,700italic,800,800italic',
    ];
    public $js = [
         "js/jquery-1.11.3.min.js",
        "web/revolution/js/jquery.themepunch.tools.min.js?rev=5.0",
        "web/revolution/js/jquery.themepunch.revolution.min.js?rev=5.0",
         "web/revolution/js/extensions/revolution.extension.slideanims.min.js",
        "web/revolution/js/extensions/revolution.extension.layeranimation.min.js",
        "web/revolution/js/extensions/revolution.extension.navigation.min.js",
        "web/revolution/js/extensions/revolution.extension.parallax.min.js" ,
        "js/bootstrap.min.js",
        "js/menuzord.js",
        "js/owl.carousel.min.js",
        "js/jquery.counterup.js",
        "js/waypoints.min.js",
        "js/countdown.js",
        "js/parallax.min.js",
         "js/jquery.bootFolio.js",
        "js/jquery.magnific-popup.js",
        "https://maps.googleapis.com/maps/api/js",
        "js/jquery-ui.js",
        "js/rev-function.js",
        "js/smoothscroll.js",
        "js/jquery.matchHeight.js",
        "js/main.js",
        "https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js",
   "https://oss.maxcdn.com/respond/1.4.2/respond.min.js",
    ];
    public $jsOptions = [
        'position' => \yii\web\View::POS_HEAD
    ];
    public $depends = [
        'yii\web\YiiAsset',
        'yii\bootstrap\BootstrapAsset',
    ];
}
