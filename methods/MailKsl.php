<?php

use yii\helpers\Url;

/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 28.07.2017
 * Time: 13:05
 */

class MailKsl
{
    public static function mail_subscription_activation ($email, $cod){

        $absoluteHomeUrl = Url::home(true); //http://ваш сайт
        $serverName = Yii::$app->request->serverName; //ваш сайт без http
        $url = $absoluteHomeUrl.'activation/'.$cod;

        $msg = "Здравствуйте! Спасибо за оформление подписки на сайте $serverName!  Вам осталось только подтвердить свой e-mail. Для этого перейдите по ссылке $url";

        $msg_html  = "<html><body style='font-family:Arial,sans-serif;'>";
        $msg_html .= "<h2 style='font-weight:bold;border-bottom:1px dotted #ccc;'>Здравствуйте! Спасибо за оформление подписки на сайте <a href='". $absoluteHomeUrl ."'>$serverName</a></h2>\r\n";
        $msg_html .= "<p><strong>Вам осталось только подтвердить свой e-mail.</strong></p>\r\n";
        $msg_html .= "<p><strong>Для этого перейдите по ссылке </strong><a href='". $url."'>$url</a></p>\r\n";
        $msg_html .= "</body></html>";

        Yii::$app->mailer->compose()
            //->setFrom('admin@klisl.com') //не надо указывать если указано в common\config\main-local.php
            ->setTo($email) // кому отправляем - реальный адрес куда придёт письмо формата asdf @asdf.com
            ->setSubject('Подтверждение подписки.') // тема письма
            ->setTextBody($msg) // текст письма без HTML
            ->setHtmlBody($msg_html) // текст письма с HTML
            ->send();
    }

}