<?php 

namespace app\models;
use yii\db\ActiveRecord;

class Reviews extends ActiveRecord
{

	public static function tableName(){
		return 'reviews';

	}
	public function attributeLabels(){
		return [
			'name' => 'Имя',
			'position' => 'Должность',
			'text' => 'Текст',
		];
	}
	public function rules(){
		return[
			[['name', 'position','text'],'required'],
		];	
	}


}