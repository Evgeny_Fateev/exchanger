<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 31.08.2017
 * Time: 13:18
 */

namespace app\models;


use yii\db\ActiveRecord;

class RedactionText extends ActiveRecord
{
    public static function tableName()
    {
        return 'redaction_text';
    }
    public function rules()
    {
        return [
            [['text', 'payment_system'],'required'],
        ];
    }
    public function attributeLabels()
    {
        return [
            'text' => 'Текст',
          'payment_system' => 'Платежная система'
        ];
    }

}