<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 27.07.2017
 * Time: 15:09
 */

namespace app\models;


use yii\db\ActiveRecord;

class Transactions extends ActiveRecord
{
    public static function tableName()
    {
        return 'transactions';
    }

    public function attributeLabels()
    {
        return [
            'wmid' => 'WMID',
            'sender_purse' => 'Кошелек отправителя',
            'recipient_purse' => 'Кошелек получателя',
            'number_transaction' => 'Номер транзакции',
            'description' => 'Описание перевода',
            'amount' => 'Сумма',
        ];
    }
    public function rules()
    {
        return [
            [['wmid','sender_purse','recipient_purse','number_transaction','description', 'amount'], 'required'],
        ];
    }

}