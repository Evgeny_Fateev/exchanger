<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 10.08.2017
 * Time: 12:19
 */

namespace app\models;


use yii\db\ActiveRecord;

class Orders extends ActiveRecord
{
    const REPLENISH = 'Пополнение';
    const OUTPUT = 'Вывод';
    public static function tableName()
    {
        return 'orders';
    }
    public function attributeLabels()
    {
        return [
            'name' => 'Ваше имя',
            'email' => 'Email',
            'phone' => 'Номер телефона',
            'type' => 'Тип',
            'payment_system' => 'Вариант платежа',
        ];
    }

    public function rules()
    {
        return[
            [['name','email','phone','type','payment_system'],'required'],
            ['type', 'in', 'range' => [self::REPLENISH, self::OUTPUT]],
            [['name','email','phone','type'], 'string', 'max' => 255],
            [['payment_system'], 'string', 'max'=> 500],
        ];
    }

    public function getUser(){
        return $this->hasOne(User::className(),['id'=>'is_user']);
    }
    public function getUserName()
    {
        $user = $this->user;

        return $user ? $user->email : '';
    }

}