<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 09.08.2017
 * Time: 16:42
 */

namespace app\models;


use yii\db\ActiveRecord;

class SubscriptionNews extends ActiveRecord
{
    public static function tableName()
    {
        return 'subscription_news';
    }
    public function rules()
    {
        return [
            [['email'],'required'],
            ['email', 'unique', 'targetClass' => '\app\models\SubscriptionNews', 'message' => 'Вы уже подписались на новости.'],
            [['email'], 'string', 'max' => 250],
        ];
    }

}