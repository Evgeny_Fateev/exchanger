<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 21.07.2017
 * Time: 14:38
 */

namespace app\models;


use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Posts extends ActiveRecord
{
    public static function tableName()
    {
        return 'posts';
    }
     public function attributeLabels()
     {
         return [
             'text' => 'Текст',
             'date' => 'Дата',
             'title' => 'Заголовок',
             'image' => 'Главное изображение',
             'category' => 'Категория',
         ];
     }
    public function rules(){
        return [
            [['image'], 'file', 'extensions' => 'png, jpg'],
            [['text', 'date','title','image','category'], 'required'],
        ];
    }
    public function upload(){
        if($this->validate()){
            $hash_name = md5(microtime() . rand(0, 9999));
            $this->image->saveAs("images_home/{$hash_name}.{$this->image->extension}");
            return "images_home/{$hash_name}.{$this->image->extension}";
        }
        else{
            return false;
        }
    }

}