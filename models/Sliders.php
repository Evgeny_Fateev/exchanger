<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 17.08.2017
 * Time: 18:32
 */

namespace app\models;

use yii\db\ActiveRecord;

class Sliders extends ActiveRecord
{
    public static function tableName()
    {
        return 'sliders';
    }
    public function rules(){
        return [
            [['image', 'text'],'required'],
            [['text'], 'string','max' => 255]
        ];
    }
    public function attributeLabels(){
        return [
            'image' => 'Изображение',
            'text' => 'Текст',
        ];
    }
    public function upload(){
        if($this->validate()){
            $hash_name = md5(microtime() . rand(0, 9999));
            $this->image->saveAs("images_home/{$hash_name}.{$this->image->extension}");
            return "images_home/{$hash_name}.{$this->image->extension}";
        }
        else{
            return false;
        }
    }

}