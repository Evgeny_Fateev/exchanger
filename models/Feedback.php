<?php
/**
 * Created by PhpStorm.
 * User: zhenj
 * Date: 27.07.2017
 * Time: 23:02
 */

namespace app\models;


use yii\db\ActiveRecord;

class Feedback extends ActiveRecord
{
    public static function tableName()
    {
        return 'feedback';
    }
    public function attributeLabels()
    {
        return [
            'name' => 'Имя',
            'email' => 'E-mail',
            'text' => 'Текст сообщения',
            'telefon' => 'Телефон',
            'title' => 'Тема',

        ];
    }

    public function rules()
    {
        return [
            [['name','email','text','telefon','title'],'required'],
            [['name', 'email', 'telefon', 'title'], 'string','max' => 50 ],
            [['text'],'string', 'max' => 500 ],
        ];
    }

}